<?php

namespace Nitra\ProductBundle\Traits;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * moderated Trait
 */
trait ModeratedDocument
{
    /**
     * @var boolean Moderated user
     * @Gedmo\Blameable(on="change", field={"isModerated"})
     * @ODM\String
     */
    protected $moderatedBy;

    /**
     * @var date $moderatedAt
     * @ODM\Date
     * @Gedmo\Timestampable(on="change", field={"isModerated"})
     */
    protected $moderatedAt;

    /**
     * @ODM\Boolean
     */
    private $isModerated;

    /**
     * Get moderatedAt
     * @return date
     */
    public function getModeratedAt()
    {
        return $this->moderatedAt;
    }

    /**
     * Get isModerated
     * @return boolean $isModerated
     */
    public function getIsModerated()
    {
        return $this->isModerated;
    }

    /**
     * Get moderatedBy.
     * @return string
     */
    public function getModeratedBy()
    {
        return $this->moderatedBy;
    }
}