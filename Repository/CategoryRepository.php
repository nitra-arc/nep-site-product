<?php

namespace Nitra\ProductBundle\Repository;

use Gedmo\Tree\Document\MongoDB\Repository\MaterializedPathRepository;

class CategoryRepository extends MaterializedPathRepository
{
    /**
     * {@inheritDoc}
     */
    public function getChildrenQueryBuilder($node = null, $direct = false, $sortByField = null, $direction = 'asc', $includeNode = false)
    {
        $qb = parent::getChildrenQueryBuilder($node, $direct, $sortByField, $direction, $includeNode);

        $qb ->sort('column', 'asc')
            ->sort('sortOrder', 'asc')
            ->sort('name', 'asc');

        return $qb;
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category|null $node
     * @return \Doctrine\MongoDB\Query\Builder
     */
    protected function getChildrenQueryBuilderWithSortOrder($node = null)
    {
        return $this->getChildrenQueryBuilder($node, null, 'level', 'desc')
            ->field('isActive')->equals(true)
            ->sort('sortOrder', 'asc')
            ->sort('name', 'asc');
    }

    /**
     * Получение массива иерархии учитывая сортировку по sortOrder
     * @param type $node
     * @return type
     */
    public function childrenHierarchyWithSortOrder($node = null)
    {
        $categories = $this->getChildrenQueryBuilderWithSortOrder($node)
            ->hydrate(false)
            ->getQuery()->execute()->toArray();

        // создаем клон категорий
        $result = $categories;
        // алиас для дочерних
        $alias = '__children';

        // верхний уровень
        $level = $node ? $node->getLevel() + 1 : 1;

        foreach ($categories as $id => $cat) {
            // добавляем алиас для всех категорий
            if (!isset($result[$id][$alias])) {
                $result[$id][$alias] = array();
            }

            // перемещаем категорию к родителю
            if ($cat['level'] > $level) {
               $parent = (string) $cat['parent']['$id'];
               $result[$parent][$alias][$id] = $result[$id];

               unset($result[$id]);
            }
        }

        return $result;
    }

    /**
     * @param string    $parentId
     * @param int       $level
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    protected function getCategories($parentId, $level)
    {
        //Получение всех категорий
        $qb = $this->createQueryBuilder($this->documentName)
            ->sort('level', 'asc')
            ->sort('column', 'asc')
            ->sort('sortOrder', 'asc')
            ->sort('name', 'asc');

        //Получение части дерева категорий с заданного уровня
        if ($parentId) {
            $qb ->field('path')->equals(new \MongoRegex('/' . $parentId . '/'))
                ->field('id')->notEqual(new \MongoId($parentId))
                ->field('level')->gte($level);
        }

        return $qb->getQuery()->execute();
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $Category
     * @param bool $active
     * @return array
     */
    protected function formatCategoryArray($Category, $active)
    {
        $result = array(
            'id'            => $Category->getId(),
            'name'          => $Category->getName(),
            'styleClass'    => $Category->getStyleClass(),
            'image'         => $Category->getImage(),
            'slug'          => $Category->getAlias(),
            'subcategories' => array(),
        );
        if ($active) {
            $result['active'] = true;
        }

        return $result;
    }

    /**
     * @param int   $level
     * @param \Nitra\ProductBundle\Document\Category $Category
     * @param bool  $active
     * @param array $result
     * @param bool  $menu
     */
    protected function addCategory($level, $Category, $active, &$result, $menu = false)
    {
        $parents    = array_reverse($this->getParents($level, $Category));
        $last       = &$result;
        foreach ($parents as $i => $parentCategory) {
            if (!$parentCategory->getIsActive()) {
                return;
            }

            $last = &$last[$parentCategory->getId()];
            if (array_key_exists($i + 1, $parents)) {
                $last = &$last['subcategories'];
                if ($menu) {
                    if (!array_key_exists((int)$parents[$i + 1]->getColumn(), $last)) {
                        $last[(int)$parents[$i + 1]->getColumn()] = array();
                    }
                    $last = &$last[(int)$parents[$i + 1]->getColumn()];
                }
            }
        }
        $last = $this->formatCategoryArray($Category, $active);
    }

    /**
     * @param \Nitra\ProductBundle\Document\Category $Category
     * @param bool $menu
     * @return bool
     */
    protected function checkCategory($Category, $menu = false)
    {
        return true;
    }

    /**
     * @param int   $level
     * @param \Nitra\ProductBundle\Document\Category $Category
     * @param array $result
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    protected function getParents($level, $Category, $result = array())
    {
        $result[$Category->getLevel()] = $Category;

        if ($Category->getParent() && $Category->getParent()->getId() && ($Category->getParent()->getLevel() > $level)) {
            return $this->getParents($level, $Category->getParent(), $result);
        } else {
            return $result;
        }
    }

    /**
     * Запрос на получение дерева категорий
     * @param string    $parentId
     * @param int       $level
     * @param string    $categoryId
     * @param int       $maxLevel
     * @return array
     */
    public function getCategoriesTree($parentId, $level = 0, $categoryId = null, $maxLevel = 3)
    {
        $categories = $this->getCategories($parentId, $level);

        //Построение выходного массива категорий
        $results = array();
        $activeCategories = $categoryId
            ? $this->getParents($level, $categories[$categoryId])
            : array();
        foreach ($categories as $category) {
            if (($category->getLevel() <= ($level + $maxLevel)) && $this->checkCategory($category)) {
                $this->addCategory($level, $category, in_array($category, $activeCategories), $results);
            }
        }
        return $results;
    }

    /**
     * Получение дерева категорий для меню
     * @param string    $parentId
     * @param int       $level
     * @param string    $categoryId
     * @param int       $maxLevel
     * @return array
     */
    public function getCategoriesTreeMenu($parentId, $level = 0, $categoryId = null, $maxLevel = 3)
    {
        $categories = $this->getCategories($parentId, $level)->toArray();

        //Построение выходного массива категорий
        $results = array();
        $activeCategories = $categoryId
            ? $this->getParents($level, $categories[$categoryId])
            : array();
        foreach ($categories as $category) {
            if (($category->getLevel() <= ($level + $maxLevel)) && $this->checkCategory($category, true)) {
                $this->addCategory($level, $category, in_array($category, $activeCategories), $results, true);
            }
        }

        return $results;
    }
}