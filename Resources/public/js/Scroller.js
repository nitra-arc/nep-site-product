/* 
 * обьект Scroller
 * @param integer currentPage - текущая страница, default = 1
 * @param string actionUrl - роутинг для получения страницы
 * @param integer maxPagesToDisplay - максимальное кол-во отображаемых страниц при скроллинге, default = 50
 * @param string buttonAddId - id кнопки "отобразить еще товаров", есле не передать, то добавление товаров будет автоматическое
 */
Scroller = function(currentPage, actionUrl, maxPagesToDisplay, buttonAddId) {
    // текущая страница
    this.currentPage = currentPage ? currentPage : 1;
    
    // url для получения следующей страницы продуктов
    this.actionUrl = actionUrl;
    
    // максимальное количество отображаемых страниц
    this.maxPagesToDisplay = maxPagesToDisplay ? maxPagesToDisplay : 10;
    
    // статус выполнения операции
    this.inProcess = false;
    
    // флаг что все страницы уже отображены
    this.noPagesToDisplay = false;
    
    this.buttonAddId =  (typeof (buttonAddId) == "undefined")
        ? false
        : buttonAddId;

    // ссылка на текущий екземпляр класса
    var _self = this;

    /**
     * подгрузка следующей страницы
     * @returns html страница продуктов | ''
     */
    if (!this.buttonAddId) {
        this.scroll = function() {
            if (!this.inProcess && !this.noPagesToDisplay && ((this.currentPage + 1) % this.maxPagesToDisplay != 0) && ($(window).scrollTop() + $(window).height()) >= ($('footer.footer').offset().top + $('footer.footer').height() * 2 - 736)) {
                this.inProcess = true;
                $('#scroll_loader').show();
                this.addPage();
                $('#scroll_loader').hide();
                this.inProcess = false;
            }
        };
    } else {
        var obj = this;
        $('#' + this.buttonAddId).click(function() {
            obj.addPage();
        });
    }

    this.addPage = function() {
        $.ajax({
            url:        this.actionUrl.replace(/&amp;/g, '&'),
            type:       'GET',
            data:       {
                page: ++ this.currentPage
            },
            dataType:   'html',
            async:      false,
            success:    function(fromServer) {
                // ответ от сервера получен успешно 
                if (fromServer && (fromServer.trim() != '')) {
                    $('div.bottom_navigation').remove();
                    $('.product_list:last').after(fromServer);
                    // заменяем верхний пагинатор
                    $('div.pagination').replaceWith($('div.bottom_navigation').children('.pagination').clone());
                    // установлление позиции всей страницы на последнюю добавленную страницу категории
                    //$(window).scrollTop($('.page_count:last').offset().top); 
                } else {
                    // все страницы были отображены
                    _self.noPagesToDisplay = true;
                }
            }
        });
    };
};