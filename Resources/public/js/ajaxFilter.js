(function( $ ){

    var settings;
    var box_selector;

    var methods = {
        init : function( options ) {
            settings = $.extend( {
                'elementSelector'           : 'a',              // селектор элементов (значний)
                'elementDisableClass'       : '.disable',       // класс не активных значений
                'elementSelectedClass'      : 'active',         // класс выбранных значений
                'elementQuantitySelector'   : 'span',           // контейнер кол-ва товаров(моделей)
                'elementQuantityNumSelector': 'span',           // контейнер числового значения кол-ва
                'elementQuantityFadeSpeed'  : 500,              // скорость исчезновения (появления) кол-ва
                'parameterNameSelector'     : '.param_name',    // селектор имени параметра
                'priceFromAlias'            : 'price-from',     // алиас цены "от" -> {{ pricer.from }
                'priceToAlias'              : 'price-to',       // алиас цены "до" -> {{ pricer.to }
                'priceFrom'                 : false,            // выбрана ли цена "от" -> {{ pricer.selected.from ? pricer.selected.from : 'false' }}
                'priceTo'                   : false,            // выбрана ли цена "до" -> {{ pricer.selected.to ? pricer.selected.to : 'false' }}
                'urlWithRouteParameters'    : '',               // массив с параметрами роута           -> {{ path('category_page', route_parameters)|url_decode }}
                'urlWithRRParameters'       : '',               // массив с параметрами роута и запроса -> {{ path('category_page', rr_parameters)|url_decode }}
                'urlWithRouteParametersAjax': '',               // массив с параметрами роута для запроса-> {{ path('category_ajax_parameters', rr_parameters)|url_decode }}
                'urlDynamicRouteGenerator'  : '',               // ссылка на генератор роутов -> "{{ path('get_dynamic_link') }}"
                'jsonRouteParameters'       : {},               // параметры роута в json формате
                'dynamicRoutePath'          : 'category_page',               // имя роута для генерации нового пути
                'clearParameterSelector'    : 'a.clear_current_parameter',
                
                'applySelector'             : '',               // селектор плавающего блока с "подобрать"
                'applyLinkSelector'         : '',               // селектор ссылки в плавающем блоке
                                                                // сеттер позиции подьезжающего элемента
                'applyPositionSetter'       : function(clickItem, settings) {
                    if((settings.applySelector != '') && (settings.applyLinkSelector != '')) {
                        var new_top = $(clickItem).position().top;
                        $(settings.applySelector).animate({top: new_top + 'px'}, 300);
                    }
                },
                                                                // функция обработки обновленной ссылки
                                                                // по умолчанию это плавающий блок
                'linkSetter'                : function(href, old_href, total) {
                    if((settings.applySelector != '') && (settings.applyLinkSelector != '')) {
                        if(href != old_href) {
                            $(settings.applySelector + ' ' + settings.applyLinkSelector).attr('href', href);
                            $(settings.applySelector).fadeIn(500);
                        } else {
                            $(settings.applySelector).fadeOut(500);
                        }
                    }
                },
                                                                // функция активации параметра
                'showParameter'             : function(parameter, settings) {
                    $(parameter).show(1000);
                },
                                                                // функция деактивации параметра
                'hideParameter'             : function(parameter, settings) {
                    $(parameter).hide(1000);
                },
                                                                // функция активации значения параметра
                'showParameterValue'        : function(value, settings) {
                    $(value).show(1000);
                },
                                                                // функция деактивации значения параметра
                'hideParameterValue'        : function(value, settings) {
                    $(value).hide(1000, function(){
                        $(this).css('display','none');
                    });
                },
                                                                // функция обновления кол-ва товаров (моделей)
                'updateQuantity'            : function(value, quantity, settings) {
                    var span = $(value).children(settings.elementQuantitySelector).children(settings.elementQuantityNumSelector);
                    if('' + quantity != span.html()) {
                        span.fadeOut(settings.elementQuantityFadeSpeed, function(){
                            $(span).html(quantity);
                            $(span).fadeIn(settings.elementQuantityFadeSpeed);
                        });
                    }
                    if(!$(value).hasClass(settings.elementSelectedClass))
                        $(value).children(settings.elementQuantitySelector).fadeIn(settings.elementQuantityFadeSpeed);
                },
                 // обновление ссылок сброса параметра
                'updateClearLink'          : function(parameter, href, settings) {
                    $(parameter).parent().find(settings.clearParameterSelector).prop('href', href);
                }
            }, options);
            
            return this.each(function(){
                box_selector = '.' + $(this).attr('class');
                methods.toggleElementActive(settings);
            });
        },
        toggleElementActive : function(settings) {
            $(box_selector + ' ' + settings.elementSelector).on('click', function() {
                if (!$(this).hasClass('settings.elementDisableClass')) {
                    if ($(settings.parameterNameSelector + '[data-alias=' + $(this).data('parameter') + '][data-multiple]').length && !$(settings.parameterNameSelector + '[data-alias=' + $(this).data('parameter') + ']').data('multiple')) {
                        var active = $(this).hasClass(settings.elementSelectedClass);
                        $(settings.elementSelector + '[data-parameter=' + $(this).data('parameter') + ']').removeClass(settings.elementSelectedClass);
                        if (active) {
                            $(this).toggleClass(settings.elementSelectedClass);
                        }
                    }
                    $(this).toggleClass(settings.elementSelectedClass);
                    if($(this).hasClass(settings.elementSelectedClass)) {
                        $(this).children(settings.elementQuantitySelector).fadeOut(settings.elementQuantityFadeSpeed);
                    } else {
                        $(this).children(settings.elementQuantitySelector).fadeIn(settings.elementQuantityFadeSpeed);
                    }
                    settings.applyPositionSetter($(this), settings);

                    methods.getParameters(settings);
                }
            });
        },
        getParameters : function(settings) {
            var parameters = {};
            $(box_selector + ' ' + settings.elementSelector + '.' + settings.elementSelectedClass).each(function() {
                if(typeof $(this).data('parameter') != "undefined") {
                    if(typeof parameters[$(this).data('parameter')] == "undefined")
                        parameters[$(this).data('parameter')] = $(this).data('value');
                    else
                        parameters[$(this).data('parameter')] += ','+$(this).data('value');
                }
            });
            if (settings.priceTo) {
                parameters[settings.priceToAlias] = settings.priceTo;
            }
            if (settings.priceFrom) {
                parameters[settings.priceFromAlias] = settings.priceFrom;
            }
            parameters = $.extend(parameters, settings.jsonRouteParameters);
            
            $.ajax({
                type: "post",
                url: settings.urlWithRouteParametersAjax,
                data: parameters,
                success: function(data) {
                    var pathname = settings.urlWithRRParameters;
                    pathname = pathname.replace(/&amp;/g, '&');
                    $.ajax({
                        type: "post",
                        url: settings.urlDynamicRouteGenerator,
                        data: {
                            'parameters': parameters,
                            'name': settings.dynamicRoutePath
                        },
                        success: function(url) {
                            settings.linkSetter(url, pathname, data.total_quantity_dcl);
                        }
                    });

                    $(box_selector + ' ' + settings.parameterNameSelector).each(function(p_i, p_item){
                        var match = false;
                        $.each(data.parameters, function(i, item){
                            if($(p_item).data('alias') == item.alias_en) {
                                settings.updateClearLink($(p_item), item.clearHref, settings);
                                var hasQuantedValues = false;
                                $.each(item.parameterValues, function(v_i, v_item){
                                    $(p_item).parent().find(settings.elementSelector).each(function(a_i, a_item){
                                        if($(this).data('value') == v_item.alias_en) {
                                            if(v_item.quantity > 0) {
                                                hasQuantedValues = true;
                                                settings.showParameterValue(a_item, settings);
                                                match = true;
                                            } else {
                                                settings.hideParameterValue(a_item, settings);
                                            }
                                            settings.updateQuantity(a_item, v_item.quantity, settings);
                                        }
                                    });
                                });
                                if (hasQuantedValues) {
                                    settings.showParameter($(p_item), settings);
                                }
                            }
                        });
                        if (!match) {
                            settings.hideParameter($(p_item), settings);
                        }
                    });
                }
            });
        }
    };

    $.fn.ajaxFilter = function( ) {
        return methods.init.apply( this, arguments );
    };
})( jQuery );