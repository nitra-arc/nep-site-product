<?php

namespace Nitra\ProductBundle\Twig\Extension;

use Nitra\ProductBundle\Breadcrumbs\Breadcrumbs;
use Nitra\ProductBundle\Lib\Filter;

class NitraExtension extends \Twig_Extension
{
    /**
     * @var \Nitra\ProductBundle\Breadcrumbs\Breadcrumbs
     */
    protected $breadcrumbs;

    /**
     * @var \Nitra\ProductBundle\Lib\Filter
     */
    protected $filter;

    /**
     * Constructor
     *
     * @param \Nitra\ProductBundle\Breadcrumbs\Breadcrumbs  $breadcrumbs
     * @param \Nitra\ProductBundle\Lib\Filter               $filter
     */
    public function __construct(Breadcrumbs $breadcrumbs, Filter $filter)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->filter      = $filter;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('breadcrumbs', array($this, 'breadcrumbs'), array(
                'is_safe' => array('all'),
            )),
            new \Twig_SimpleFunction('filter', array($this, 'filter'), array(
                'is_safe' => array('all'),
            )),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            'unset' => new \Twig_Filter_Method($this, 'unsetArrayElement'),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'nitra_product_extensions';
    }

    /**
     * Unset array item by key
     *
     * @param array $arr
     * @param string|int $key
     *
     * @return array
     */
    public function unsetArrayElement($arr, $key)
    {
        if (array_key_exists($key, $arr)) {
            unset($arr[$key]);
        }

        return $arr;
    }

    /**
     * Render breadcrumbs
     *
     * @param object $object
     * @param string $template
     *
     * @return string Html
     */
    public function breadcrumbs($object, $template = null)
    {
        if ($template) {
            $this->breadcrumbs->setDefaultTemplate($template);
        }

        return $this->breadcrumbs->render($object);
    }

    /**
     * Render filter
     *
     * @param string $template
     *
     * @return string Html
     */
    public function filter($template = null)
    {
        return $this->filter->render($template);
    }
}