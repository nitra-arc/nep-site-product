<?php

namespace Nitra\ProductBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Nitra\ProductBundle\DependencyInjection\CompilerPass\BreadcrumbsCompilerPass;

class NitraProductBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new BreadcrumbsCompilerPass());
    }
}