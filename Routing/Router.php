<?php

namespace Nitra\ProductBundle\Routing;

use Symfony\Bundle\FrameworkBundle\Routing\Router as BaseRouter;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Router extends BaseRouter
{
    /** @var \Doctrine\ODM\MongoDB\DocumentManager */
    protected $dm;

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /** @var \Symfony\Component\Routing\RequestContext */
    protected $context;

    /**
     * {@inheritdoc}
     */
    public function __construct(ContainerInterface $container, $resource, array $options = array(), RequestContext $context = null, $logger = null)
    {
        parent::__construct($container, $resource, $options, $context, $logger);

        $this->container = $container;
        $this->dm        = $container->get('doctrine_mongodb.odm.document_manager');
        $this->context   = $context;
    }

    /**
     * {@inheritdoc}
     */
    public function generate($name, $parameters = array(), $referenceType = self::ABSOLUTE_PATH)
    {
        if ($this->getConfig('enabled')) {
            return $this->generateSemantic($name, $parameters, $referenceType);
        }

        return $this->getGenerator()->generate($name, $parameters, $referenceType);
    }

    /**
     * Generate semantic link
     *
     * @param string            $name
     * @param array             $parameters
     * @param boolean|string    $referenceType
     *
     * @return string Generated link
     */
    protected function generateSemantic($name, $parameters, $referenceType)
    {
        $method = 'generateSemantic' . $this->generateMethodByName($name);

        if (method_exists($this, $method) && $link = $this->$method($parameters)) {
            $home = $this->getGenerator()->generate('nitra_store_home_index', array(), $referenceType);

            return $home . $link;
        }

        return $this->getGenerator()->generate($name, $parameters, $referenceType);
    }

    /**
     * Generate method for call
     *
     * @param string $name  Route name
     *
     * @return null|string
     */
    protected function generateMethodByName($name)
    {
        $method = null;
        foreach (explode('_', $name) as $nameItem) {
            $method .= ucfirst($nameItem);
        }

        return $method;
    }

    /**
     * {@inheritdoc}
     */
    public function match($pathInfo)
    {
        try {
            return $this->getMatcher()->match($pathInfo);
        } catch (\Symfony\Component\Routing\Exception\ResourceNotFoundException $exception) {
            if ($this->getConfig('enabled') && $matched = $this->matchSemantic($pathInfo)) {
                return $matched;
            }

            throw $exception;
        }
    }

    /**
     * Match semantic link
     *
     * @param string            $pathInfo
     *
     * @return array An array of parameters
     */
    protected function matchSemantic($pathInfo)
    {
        if ($articleCategory = $this->matchSemanticInformationCategoryPage($pathInfo)) {
            return $articleCategory;
        }
        if ($article = $this->matchSemanticInformationPage($pathInfo)) {
            return $article;
        }
        if ($category = $this->matchSemanticCategoryPage($pathInfo)) {
            return $category;
        }
        if ($model = $this->matchSemanticModelPage($pathInfo)) {
            return $model;
        }
        if ($product = $this->matchSemanticProductPage($pathInfo)) {
            return $product;
        }

        return array();
    }

    /**
     * Get config by key
     *
     * @param string $key
     *
     * @return mixed
     */
    protected function getConfig($key)
    {
        return $this->container->getParameter('nitra.router.' . $key);
    }

    /**
     * Generate semantic link to category page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticCategoryPage($parameters)
    {
        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('fullUrlAliasEn')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();
        if (!$category) {
            return null;
        }

        unset($parameters['slug']);

        $queryParameters = http_build_query($parameters);

        $query = $queryParameters
            ? ('?' . $queryParameters)
            : null;

        return $category['fullUrlAliasEn'] . $query;
    }

    /**
     * Match semantic category page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticCategoryPage($pathInfo)
    {
        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('aliasEn')
            ->field('fullUrlAliasEn')->equals(trim($pathInfo, '/'))
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        $sort = null;
        if ($this->container->isScopeActive('request') && $sort = $this->container->get('request')->query->get('sort')) {
            $sort = '/' . $sort;
        }
        $parameters = $this->getMatcher()->match('/category/__slug__' . $sort);
        $parameters['slug'] = $category['aliasEn'];

        return $parameters;
    }

    /**
     * Generate semantic link to model page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticModelPage($parameters)
    {
        $model = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('category')
            ->field('aliasEn')->equals($parameters['alias'])
            ->getQuery()->getSingleResult();

        if (!$model) {
            return null;
        }

        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('fullUrlAliasEn')
            ->field('id')->equals($model['category']['$id'])
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        return $category['fullUrlAliasEn'] . '/' . $parameters['alias'];
    }

    /**
     * Match semantic category page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticModelPage($pathInfo)
    {
        $matches = array();
        if (!preg_match('/\/(.*)\/(.*)$/', $pathInfo, $matches)) {
            return null;
        }
        list (, $categoryAlias, $modelAlias) = $matches;

        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('_id')
            ->field('fullUrlAliasEn')->equals($categoryAlias)
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        $model = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('aliasEn')
            ->field('aliasEn')->equals($modelAlias)
            ->field('category.$id')->equals($category['_id'])
            ->getQuery()->getSingleResult();

        if (!$model) {
            return null;
        }

        return $this->getMatcher()->match('/model/' . $modelAlias);
    }

    /**
     * Generate semantic link to product page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticProductPage($parameters)
    {
        $product = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('fullUrlAliasEn')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();

        if (!$product) {
            return null;
        }

        return $product['fullUrlAliasEn'];
    }

    /**
     * Match semantic product page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticProductPage($pathInfo)
    {
        $matches = array();
        if (!preg_match('/\/(.*)\/(.*)\/(.*)$/', $pathInfo, $matches)) {
            return null;
        }
        list (, $categoryAlias, $modelAlias, $productAlias) = $matches;

        $category = $this->dm->createQueryBuilder('NitraProductBundle:Category')
            ->hydrate(false)->select('_id')
            ->field('fullUrlAliasEn')->equals($categoryAlias)
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        $model = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->hydrate(false)->select('_id')
            ->field('aliasEn')->equals($modelAlias)
            ->field('category.$id')->equals($category['_id'])
            ->getQuery()->getSingleResult();

        if (!$model) {
            return null;
        }

        $product = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('aliasEn')
            ->field('model.$id')->equals($model['_id'])
            ->field('aliasEn')->equals($productAlias)
            ->getQuery()->getSingleResult();

        if (!$product) {
            return null;
        }

        return $this->getMatcher()->match('/product/' . $productAlias);
    }

    /**
     * Generate semantic link to category page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticInformationCategoryPage($parameters)
    {
        $category = $this->dm->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->hydrate(false)->select('aliasEn')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        return $category['aliasEn'];
    }

    /**
     * Match semantic category page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticInformationCategoryPage($pathInfo)
    {
        $category = $this->dm->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->hydrate(false)->select('aliasEn')
            ->field('aliasEn')->equals(trim($pathInfo, '/'))
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        return $this->getMatcher()->match('/information-category-page/' . trim($pathInfo, '/'));
    }

    /**
     * Generate semantic link to category page
     *
     * @param array $parameters
     *
     * @return null|string
     */
    protected function generateSemanticInformationPage($parameters)
    {
        $article = $this->dm->createQueryBuilder('NitraInformationBundle:Information')
            ->hydrate(false)->select('informationCategory')
            ->field('aliasEn')->equals($parameters['slug'])
            ->getQuery()->getSingleResult();

        if (!$article) {
            return null;
        }

        $category = $this->dm->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->hydrate(false)->select('aliasEn')
            ->field('_id')->equals($article['informationCategory']['$id'])
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        return $category['aliasEn'] . '/' . $parameters['slug'];
    }

    /**
     * Match semantic category page
     *
     * @param string $pathInfo
     *
     * @return array|null
     */
    protected function matchSemanticInformationPage($pathInfo)
    {
        if (!preg_match('/\/.*\/.*\//', $pathInfo)) {
            return null;
        }
        list (, $categoryAlias, $articleAlias) = explode('/', $pathInfo);

        $category = $this->dm->createQueryBuilder('NitraInformationBundle:InformationCategory')
            ->hydrate(false)->select('_id')
            ->field('aliasEn')->equals($categoryAlias)
            ->getQuery()->getSingleResult();

        if (!$category) {
            return null;
        }

        $article = $this->dm->createQueryBuilder('NitraInformationBundle:Information')
            ->hydrate(false)->select('aliasEn')
            ->field('aliasEn')->equals($articleAlias)
            ->field('informationCategory.$id')->equals($category['_id'])
            ->getQuery()->getSingleResult();

        if (!$article) {
            return null;
        }

        return $this->getMatcher()->match('/information-page/' . $articleAlias);
    }
}