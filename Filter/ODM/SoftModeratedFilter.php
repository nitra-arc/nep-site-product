<?php

namespace Nitra\ProductBundle\Filter\ODM;

use Doctrine\ODM\MongoDB\Query\Filter\BsonFilter;
use Doctrine\ODM\MongoDB\Mapping\ClassMetadata;

class SoftModeratedFilter extends BsonFilter
{
    /**
     * Gets the criteria part to add to a query.
     *
     * @return array The criteria array, if there is available, empty array otherwise
     */
    public function addFilterCriteria(ClassMetadata $targetEntity)
    {
        $traitNames = $targetEntity->reflClass->getTraitNames();

        if (!in_array('Nitra\ProductBundle\Traits\ModeratedDocument', $traitNames)) {
            return '';
        }

        return array(
            'isModerated' => true
        );
    }
}