<?php

namespace Nitra\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('term', 'text', array(
            'required'      => false,
            'label'         => ' ',
            'attr'          => array(
                'class'         => 'nitra_search_suggest',
            ),
        ));
        $builder->add('search', 'button', array(
            'label'         => 'search.button',
            'attr'          => array(
                'class'         => 'js-click-on-search',
            ),
        ));
    }
    
    public function getName()
    {
        return 'nitra_search';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'NitraProductBundle',
        ));
    }
}