<?php

namespace Nitra\ProductBundle\Form\Type\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;
use Nitra\StoreBundle\Lib\Globals;

/**
 * Форма расчета стоимости доставки Подукта
 */
class ProductEstimateDeliveryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // массив городов
        $citiesChoices          = array();
        $citiesIdsForConstrait  = array();
        if (isset($options['citiesTetradka']) && $options['citiesTetradka']) {
            foreach($options['citiesTetradka']['cities'] as $city) {
                if (!key_exists($city['regionName'], $citiesChoices)) {
                    $citiesChoices[$city['regionName']] = array();
                }
                $citiesIdsForConstrait[] = $city['businessKey'];
                $citiesChoices[$city['regionName']][$city['businessKey']] = $city['name'];
            }
        }
        $translator          = Globals::$container->get('translator');
        $select2NoMatches    = $translator->trans('select2.formatNoMatches', array(), 'NitraStoreBundle');
        $select2ShortInput   = $translator->trans('select2.formatInputTooShort', array(), 'NitraStoreBundle');
        $select2BigSelection = $translator->trans('select2.formatSelectionTooBig', array(), 'NitraStoreBundle');

        // город получатель
        $builder->add('city', 'genemu_jqueryselect2_choice', array(
            'choices'     => $citiesChoices,
            'required'    => true,
            'empty_value' => '',
            'data'        => null,
            'label'       => 'product.delivery.city.label',
            'help'        => 'product.delivery.city.help',
            'constraints' => array(
                new Constraints\Choice($citiesIdsForConstrait),
            ),
            'configs'     => array(
                'minimumInputLength'    => 1,
                'matcher'               => 'function(term, text, opt) {
                    var e = new RegExp("^" + term, "i");
                    return text.match(e);
                }',
                'placeholder'           => '',
                'formatNoMatches'       => 'function (input, min) { var n = min - input.length; return "' . $select2NoMatches . '"; }',
                'formatSelectionTooBig' => 'function (n) { return "' . $select2BigSelection . '"; }',
                'formatInputTooShort'   => 'function (input, min) { var n = min - input.length; return "' . $select2ShortInput . '"; }',
            ),
        ));

        // ID продукта
        $builder->add('productId', 'hidden', array(
            'required' => true,
        ));

        // кнопка расчтиать
        $builder->add('button_estimate', 'button', array(
            'label' => 'product.delivery.button_estimate',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'product_estimate_delivery';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // установить данные по умолчанию
        $resolver->setDefaults(array(
            // города
            'citiesTetradka'     => array(),
            // данные формы
            'data'               => array(
                // Дата отправки
                'date' => new \DateTime(),
            ),
            'translation_domain' => 'NitraProductBundle',
        ));

        // установить обязательные параметры
        $resolver->setRequired(array(
            'citiesTetradka',
        ));
    }
}