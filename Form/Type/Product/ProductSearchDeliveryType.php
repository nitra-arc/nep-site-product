<?php

namespace Nitra\ProductBundle\Form\Type\Product;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

/**
 * Форма поиска продукта для последующего расчета доставки
 */
class ProductSearchDeliveryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // артикул товара
        $builder->add('article', 'text', array(
            'required'    => true,
            'label'       => 'product.article',
            'constraints' => array(
                new Constraints\NotBlank(),
            )
        ));

        // кнопка поиска
        $builder->add('button_search', 'button', array(
            'label' => 'product.delivery.button_search',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'product_search_delivery';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        // установить данные по умолчанию
        $resolver->setDefaults(array(
            // города
            'citiesTetradka'     => array(),
            // данные формы
            'data'               => array(
                // Дата отправки
                'date' => new \DateTime(),
            ),
            'translation_domain' => 'NitraProductBundle',
        ));

        // установить обязательные параметры
        $resolver->setRequired(array(
            'citiesTetradka',
        ));
    }
}