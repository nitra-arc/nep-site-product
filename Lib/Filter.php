<?php

namespace Nitra\ProductBundle\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Nitra\StoreBundle\Lib\Globals;

class Filter
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface   Container instance
     */
    protected $container;

    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager Document manager
     */
    protected $dm;

    /**
     * @var \Twig_Environment  Twig environment instance
     */
    protected $twig;

    /**
     * @var \Doctrine\ODM\MongoDB\Query\Builder[] List of query builders for replace selected parameters
     * For when select value, in filter be more allowed values
     */
    protected $qbs = array();

    /**
     * @var array Query without conditions
     */
    protected $defaultQueryArrayWithoutConditions;

    /**
     * @var array Collected parameters
     */
    protected $parameters = array();

    /**
     * @var array Route parameters
     */
    protected $routeParameters = array();

    /**
     * @var string Default twig template
     */
    protected $defaultTemplate = 'NitraProductBundle:Filters:%s.html.twig';

    /**
     * @var string Mode for filtering
     * model and product is allowed
     */
    protected $mode;

    /**
     * @var array Default options for each filter
     */
    protected $defaultFilterOptions = array(
        'suffix'         => '',
        'isSiteMultiple' => true,
        'isFilterShow'   => true,
        'values'         => array(),
        'order'          => 0,
    );

    /**
     * @var string  Map function for mongodb for collecting parameters
     */
    protected $mapFunction = 'function() {
        if ("__field__" != "price" || "__field__" == "") {
            for (var i in this.parameters) {
                var parameter = this.parameters[i];
                if ("__field__" == "" || parameter.parameter.valueOf() == "__field__") {
                    var values = {};
                    for (var j in parameter.values) {
                        values[parameter.values[j].valueOf()] = 1;
                    }
                    var params = {};
                    params[parameter.parameter.valueOf()] = values;
                    emit(this.model["$id"].valueOf(), params);
                }
            }
        }
        if ("__field__" == "price" || "__field__" == "") {
            emit(this.model["$id"].valueOf(), {"price": this.storePrice["__storeId__"].price});
        }
    }';

    /**
     * @var string  Reduce function for mongodb for collecting parameters
     */
    protected $reduceFunction = 'function(key, value) {
        return {parameters: value};
    }';

    /**
     * @var string  Map function for mongodb for collect brands of models
     */
    protected $modelBrandsMapFunction = 'function() {
        var modelsProductsAmounts = __amounts__;
        var amount = modelsProductsAmounts[this._id.valueOf()];
        emit(this.brand["$id"].valueOf(), amount);
    }';

    /**
     * @var string  Reduce function for mongodb for collect brands of models
     */
    protected $modelBrandsReduceFunction = 'function(key, value) {
        return Array.sum(value);
    }';

    /**
     * Constructor
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->dm        = $container->get('doctrine_mongodb.odm.document_manager');
        $this->twig      = $container->get('twig');

        $this->mode      = $container->getParameter('nitra.filter.mode');
    }

    /**
     * Aggregate parameters from products
     *
     * @param \Nitra\ProductBundle\Document\Category    $category   Category instance
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder Modified builder (with conditions from query)
     */
    public function filter($category)
    {
        $this->routeParameters = $this->getRouteParameters();

        $qb = $this->getProductsQb($category);

        $this->parseRequest($qb);

        $values = $this->collectValues($qb);

        $this->parameters = $this->replaceSelectedValues($values);

        return $this->returnQb($qb);
    }

    /**
     * Get parameters for ajax
     *
     * @param \Nitra\ProductBundle\Document\Category
     *
     * @return array
     */
    public function ajax($category)
    {
        $this->routeParameters = $this->getRouteParameters();

        $qb = $this->getProductsQb($category);

        $this->parseRequest($qb);

        $values = $this->collectValues($qb);

        $totalAmount = $this->returnQb($qb)
            ->getQuery()->execute()->count();

        $bent = $this->getDeclensionTwigExtension()->declension($totalAmount, 'one', 'two', 'many', false);

        $translated = $this->getTranslator()->trans('filters.dcl.' . $this->mode . '.' . $bent, array(
            '%q%'   => $totalAmount,
        ), 'NitraProductBundle');

        return array(
            'parameters' => $this->replaceSelectedValues($values),
            'total'      => $translated,
        );
    }

    /**
     * Render filter
     *
     * @param string $template
     *
     * @return string Html
     */
    public function render($template)
    {
        $templateToRender = $template
            ?:
            sprintf($this->defaultTemplate, $this->container->getParameter('nitra.filter.template'));

        $context          = $this->getTemplateContext();

        return $this->twig->render($templateToRender, $context);
    }

    /**
     * Get store from Globals
     *
     * @return array
     */
    protected function getStore()
    {
        return Globals::getStore();
    }

    /**
     * Get translator instance
     *
     * @return \Symfony\Bundle\FrameworkBundle\Translation\Translator
     */
    protected function getTranslator()
    {
        return $this->container->get('translator');
    }

    /**
     * Get declension twig extension
     *
     * @return \Nitra\StoreBundle\Twig\Extension\NitraExtension
     */
    protected function getDeclensionTwigExtension()
    {
        return $this->container->get('nitra_extension');
    }

    /**
     * Get request instance if is active
     *
     * @return \Symfony\Component\HttpFoundation\Request|null
     */
    protected function getRequest()
    {
        return $this->container->isScopeActive('request')
            ? $this->container->get('request')
            : null;
    }

    /**
     * Get product repository
     *
     * @return \Nitra\ProductBundle\Repository\ProductRepository
     */
    protected function getProductRepository()
    {
        return $this->dm->getRepository('NitraProductBundle:Product');
    }

    /**
     * Return query builder by mode
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb     Query builder instance
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    protected function returnQb($qb)
    {
        $builder = null;
        switch ($this->mode) {
            case 'product':
                $builder = $this->dm->createQueryBuilder('NitraProductBundle:Product')
                    ->setQueryArray($qb->getQueryArray());
                break;
            case 'model':
                $modelsIds = $qb
                    ->distinct('model.$id')
                    ->getQuery()->execute()->toArray();

                $builder = $this->dm->createQueryBuilder('NitraProductBundle:Model')
                    ->field('id')->in($modelsIds);
                break;
        }

        return $builder;
    }

    /**
     * Collect values from products
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb     Query builder instance
     * @param string                                $field  Field which be collected
     *
     * @return array Collected values
     */
    protected function collectValues($qb, $field = null)
    {
        if ($field == 'brand') {
            return array(
                'brand' => $this->collectBrands($qb),
            );
        }

        $store  = $this->getStore();

        $replacements = array(
            '__storeId__' => $store['id'],
            '__field__'   => $field,
        );

        $map    = str_replace(array_keys($replacements), array_values($replacements), $this->mapFunction);
        $reduce = $this->reduceFunction;

        $result = $qb
            ->map($map)
            ->reduce($reduce)
            ->getQuery()->execute()->toArray();

        $formatted = $this->formatCollectedValues($result);
        $formatted['brand'] = $this->collectBrands($qb);

        return $formatted;
    }

    /**
     * Format collected values
     *
     * @param array $collected
     *
     * @return array Formatted result
     */
    protected function formatCollectedValues($collected)
    {
        $result = array(
            'price' => array(),
        );
        foreach ($collected as $parameters) {
            if (!array_key_exists('parameters', $parameters['value'])) {
                $parameters['value'] = array(
                    'parameters' => array(
                        $parameters['value'],
                    ),
                );
            }
            foreach ($parameters['value']['parameters'] as $item) {
                foreach ($item as $parameterId => $values) {
                    if ($parameterId == 'price') {
                        $result['price'][] = $values;
                        continue;
                    }
                    $this->processCollectedParameterValues($parameterId, $result, $values, $parameters);
                }
            }
        }
        if ($this->mode == 'model') {
            $this->collectModelValues($result);
        }

        sort($result['price'], SORT_NUMERIC);

        return $result;
    }

    /**
     * Process formatting of parameter values
     *
     * @param string    $parameterId        Id of parameter
     * @param array     $result             Result array
     * @param array     $values             Values list
     * @param array     $parameters         Product or model parameters
     */
    protected function processCollectedParameterValues($parameterId, &$result, $values, $parameters)
    {
        // if parameter id is not defined in result array
        if (!array_key_exists($parameterId, $result)) {
            // define him
            $result[$parameterId] = array();
        }
        // iterate values
        foreach ($values as $valueId => $amount) {
            // switch by mode
            switch ($this->mode) {
                // if mode is product
                case 'product':
                    // if value id is not defined in result array by parameter id
                    if (!array_key_exists($valueId, $result[$parameterId])) {
                        // define him with zero value
                        $result[$parameterId][$valueId] = 0;
                    }
                    // increment amount to result array
                    $result[$parameterId][$valueId] += $amount;
                    break;
                // if mode is model
                case 'model':
                    // if value id is not defined in result array by parameter id
                    if (!array_key_exists($parameters['_id'], $result[$parameterId])) {
                        // define him with empty array
                        $result[$parameterId][$parameters['_id']] = array();
                    }
                    // set amount
                    $result[$parameterId][$parameters['_id']][$valueId] = $amount;
                    break;
            }
        }
    }

    /**
     * Collect parameter value for model mode
     *
     * @param array $result
     */
    protected function collectModelValues(&$result)
    {
        // iterate results
        foreach ($result as $parameterId => $models) {
            // skip price parameter
            if ($parameterId == 'price') {
                continue;
            }
            // define empty values array
            $newValues = array();
            // iterate models
            foreach ($models as $modelId => $values) {
                // iterate values
                foreach ($values as $valueId => $amount) {
                    // if value id is not defined in values array
                    if (!array_key_exists($valueId, $newValues)) {
                        $newValues[$valueId] = 0;
                    }

                    // increment amount to values array by value id
                    $newValues[$valueId] += $amount;
                }
            }
            // set new values to result array
            $result[$parameterId] = $newValues;
        }
    }

    /**
     * Collect brands from models
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder $qb
     *
     * @return array Collected brands
     */
    protected function collectBrands($qb)
    {
        $models = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->hydrate(false)->select('model')
            ->setQueryArray($qb->getQueryArray())
            ->getQuery()->execute();

        $modelsIds = array();
        foreach ($models as $model) {
            $id = (string) $model['model']['$id'];

            if ($this->mode == 'model') {
                $modelsIds[$id] = 1;
            } else {
                if (!array_key_exists($id, $modelsIds)) {
                    $modelsIds[$id] = 0;
                }
                $modelsIds[$id] ++;
            }
        }

        $map    = str_replace('__amounts__', json_encode($modelsIds), $this->modelBrandsMapFunction);
        $reduce = $this->modelBrandsReduceFunction;

        $brands = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->field('id')->in(array_keys($modelsIds))
            ->map($map)
            ->reduce($reduce)
            ->getQuery()->execute()->toArray();

        $result = array();
        foreach ($brands as $brand) {
            $result[$brand['_id']] = $brand['value'];
        }

        return $result;
    }

    /**
     * Get query builder for products
     *
     * @param \Nitra\ProductBundle\Document\Category    $category  Category instance
     *
     * @return \Doctrine\ODM\MongoDB\Query\Builder
     */
    protected function getProductsQb($category)
    {
        $qb = $this->getProductRepository()
            ->getProductsByCategoryQb($category);

        $this->defaultQueryArrayWithoutConditions = $qb->getQueryArray();

        return $qb;
    }

    /**
     * Get parameters from query ($_GET array)
     *
     * @return string|null  Query parameters
     */
    protected function getQueryParameters()
    {
        return $this->container->isScopeActive('request')
            ? $this->container->get('request')->query->all()
            : null;
    }

    /**
     * Parse request and add conditions to query
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder $qb
     */
    protected function parseRequest($qb)
    {
        foreach ($this->getQueryParameters() as $parameter => $value) {
            switch ($parameter) {
                case 'price-from':
                    $this->addPriceCondition($qb, $value);
                    break;
                case 'price-to':
                    $this->addPriceCondition($qb, null, $value);
                    break;
                case 'brand':
                    $this->addBrandCondition($qb, $value);
                    break;
                case 'color':
                    $this->addColorCondition($qb, $value);
                    break;
                default:
                    $this->addParameterCondition($qb, $parameter, $value);
                    break;
            }
        }
    }

    /**
     * Add prices conditions to query
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb             Query builder instance
     * @param float                                 $priceFrom      Price from
     * @param float                                 $priceTo        Price to
     */
    protected function addPriceCondition($qb, $priceFrom = null, $priceTo = null)
    {
        if (!array_key_exists('price', $this->qbs)) {
            foreach ($this->qbs as $additionalQb) {
                $this->priceCondition($additionalQb, $priceFrom, $priceTo);
            }

            $this->qbs['price'] = clone $qb;
        }

        $this->priceCondition($qb, $priceFrom, $priceTo);
    }

    /**
     * Add prices conditions to query
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb             Query builder instace
     * @param float                                 $priceFrom      Price from
     * @param float                                 $priceTo        Price to
     */
    protected function priceCondition($qb, $priceFrom, $priceTo)
    {
        $store = $this->getStore();

        $qb->field('storePrice.' . $store['id'] . '.price');

        if (!is_null($priceFrom)) {
            $qb->gte((float) $priceFrom);
        }
        if (!is_null($priceTo)) {
            $qb->lte((float) $priceTo);
        }
    }

    /**
     * Add brand condition to query
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb             Query builder instaNce
     * @param string                                $selectedBrands Selected brands
     */
    protected function addBrandCondition($qb, $selectedBrands)
    {
        $brands = $this->dm->createQueryBuilder('NitraProductBundle:Brand')
            ->distinct('_id')
            ->field('aliasEn')->in(explode(',', $selectedBrands))
            ->getQuery()->execute()->toArray();

        $models = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('_id')
            ->field('id')->in($qb->getQueryArray()['model.$id']['$in'])
            ->field('brand.id')->in($brands)
            ->getQuery()->execute()->toArray();

        foreach ($this->qbs as $additionalQb) {
            $additionalQb->field('model.id')->in($models);
        }

        $this->qbs['brand'] = clone $qb;

        $qb->field('model.id')->in($models);
    }

    /**
     * Add color conditions to query
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb             Query builder instance
     * @param string                                $selectedColors Selected colors
     */
    protected function addColorCondition($qb, $selectedColors)
    {
        $colors = $this->dm->createQueryBuilder('NitraProductBundle:Color')
            ->distinct('_id')
            ->field('aliasEn')->in(explode(',', $selectedColors))
            ->getQuery()->execute()->toArray();

        foreach ($this->qbs as $additionalQb) {
            $additionalQb->field('color.id')->in($colors);
        }

        $this->qbs['color'] = clone $qb;

        $qb->field('color.id')->in($colors);
    }

    /**
     * Add parameter condition
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder   $qb             Query builder instance
     * @param string                                $alias          Parameter alias
     * @param string                                $values         Values aliases
     *
     * @return null
     */
    protected function addParameterCondition($qb, $alias, $values)
    {
        if (!$parameter = $this->findIsFilterParameterByAlias($alias)) {
            return;
        }
        if (!$values = $this->findParameterValuesByAliases($parameter->getId(), explode(',', $values))) {
            return;
        }

        $valuesIds = array();
        foreach ($values as $value) {
            $valuesIds[] = new \MongoId($value->getId());
        }

        $expr = $qb->expr()
            ->field('parameters')->elemMatch($qb->expr()
                ->field('parameter')->equals(new \MongoId($parameter->getId()))
                ->field('values')->in($valuesIds)
            );

        foreach ($this->qbs as $additionalQb) {
            $additionalQb->addOr($expr);
        }

        $this->qbs[$parameter->getId()] = clone $qb;

        $qb->addOr($expr);
    }

    /**
     * Replace selected values as values without each filter
     *
     * @param array $values
     *
     * @return array Replaced values
     */
    protected function replaceSelectedValues($values)
    {
        foreach ($values as $key => &$value) {
            if (!array_key_exists($key, $this->qbs)) {
                continue;
            }

            $value = $this->collectValues($this->qbs[$key], $key)[$key];
        }

        return $values;
    }

    /**
     * Get template parameters
     *
     * @return array Context
     */
    protected function getTemplateContext()
    {
        $context = $this->getDefaultContext();

        foreach ($this->parameters as $key => $value) {
            switch ($key) {
                case 'price':
                    $context['prices'] = $value;
                    break;
                case 'brand':
                    $brandFilter = $this->getBrandContext($value);
                    if ($brandFilter) {
                        $context['filters']['filters.brand'] = $brandFilter;
                    }
                    break;
                case 'color':
                    $colorFilter = $this->getColorContext($value);
                    if ($colorFilter) {
                        $context['filters']['filters.color'] = $colorFilter;
                    }
                    break;
                default:
                    $this->addParameterContext($key, $value, $context['filters']);
                    break;
            }
        }

        $this->sortFilters($context['filters']);

        return $context;
    }

    /**
     * Get route parameters from request
     *
     * @return array
     */
    protected function getRouteParameters()
    {
        $request = $this->getRequest();
        if (!$request) {
            return array();
        }

        return array_merge(
            $request->query->all(),
            $request->attributes->get('_route_params')
        );
    }

    /**
     * Get default context for template
     *
     * @return array
     */
    protected function getDefaultContext()
    {
        return array(
            'routeParameters' => $this->routeParameters,
            'filters'         => array(),
            'selectedBox'     => $this->container->getParameter('nitra.filter.selected_box'),
        );
    }

    /**
     * Get brands for template
     *
     * @param array $value
     *
     * @return array
     */
    protected function getBrandContext($value)
    {
        $result = array(
            'active'         => array_key_exists('brand', $this->routeParameters),
            'alias'          => 'brand',
        ) + $this->defaultFilterOptions;

        $selectedBrands = $result['active']
            ? explode(',', $this->routeParameters['brand'])
            : array();

        foreach ($value as $id => $amount) {
            $brand = $this->dm->find('NitraProductBundle:Brand', $id);
            $result['values'][] = $this->formatReferenceValue($brand, $selectedBrands, $amount);
        }

        $result['values'] = array_merge(
            $result['values'],
            $this->appendEmptyBrands($result['values'])
        );

        return $result['values'] ? $result : null;
    }

    /**
     * Add empty brands to result array
     *
     * @param array $existsValues
     *
     * @return array
     */
    protected function appendEmptyBrands($existsValues)
    {
        $nonEmptyBrands = array();
        foreach ($existsValues as $existsValue) {
            $nonEmptyBrands[] = new \MongoId($existsValue['id']);
        }

        $modelsIds = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->setQueryArray($this->defaultQueryArrayWithoutConditions)
            ->distinct('model.$id')
            ->getQuery()->execute()->toArray();

        $ids = $this->dm->createQueryBuilder('NitraProductBundle:Model')
            ->distinct('brand.$id')
            ->field('_id')->in($modelsIds)
            ->getQuery()->execute()->toArray();

        $brands = $this->dm->createQueryBuilder('NitraProductBundle:Brand')
            ->field('_id')
                ->in($ids)
                ->notIn($nonEmptyBrands)
            ->getQuery()->execute();

        $result = array();
        foreach ($brands as $brand) {
            $result[] = $this->formatReferenceValue($brand);
        }

        return $result;
    }

    /**
     * Get colors for template
     *
     * @param array $value
     *
     * @return array
     */
    protected function getColorContext($value)
    {
        $result = array(
            'active'         => array_key_exists('color', $this->routeParameters),
            'alias'          => 'color',
        ) + $this->defaultFilterOptions;

        $selectedColors = $result['active']
            ? explode(',', $this->routeParameters['color'])
            : array();

        foreach ($value as $id => $amount) {
            $color = $this->dm->find('NitraProductBundle:Color', $id);
            $result['values'][] = $this->formatReferenceValue($color, $selectedColors, $amount);
        }

        $result['values'] = array_merge(
            $result['values'],
            $this->appendEmptyColors($result['values'])
        );

        return $result['values'] ? $result : null;
    }

    /**
     * Add empty colors to result array
     *
     * @param array $existsValues
     *
     * @return array
     */
    protected function appendEmptyColors($existsValues)
    {
        $nonEmptyColors = array();
        foreach ($existsValues as $existsValue) {
            $nonEmptyColors[] = new \MongoId($existsValue['id']);
        }

        $ids = $this->dm->createQueryBuilder('NitraProductBundle:Product')
            ->setQueryArray($this->defaultQueryArrayWithoutConditions)
            ->distinct('color.$id')
            ->getQuery()->execute()->toArray();

        $colors = $this->dm->createQueryBuilder('NitraProductBundle:Color')
            ->field('_id')
                ->in($ids)
                ->notIn($nonEmptyColors)
            ->getQuery()->execute();

        $result = array();
        foreach ($colors as $color) {
            $result[] = $this->formatReferenceValue($color);
        }

        return $result;
    }

    /**
     * Formatting value to result array
     *
     * @param \Nitra\ProductBundle\Document\Brand|\Nitra\ProductBundle\Document\Color   $value              Value
     * @param array                                                                     $selectedValues     Selected values aliases
     * @param integer                                                                   $amount             Amount
     *
     * @return array
     */
    protected function formatReferenceValue($value, $selectedValues = array(), $amount = 0)
    {
        return array(
            'active' => in_array($value->getAlias(), $selectedValues),
            'amount' => $amount,
            'name'   => (string) $value,
            'alias'  => $value->getAlias(),
            'id'     => $value->getId(),
        );
    }

    /**
     * Add parameter to context
     *
     * @param string    $id                 Parameter id
     * @param array     $valuesWithAmounts  List of values
     * @param array     $filters            Link to filters
     *
     * @return null
     */
    protected function addParameterContext($id, $valuesWithAmounts, &$filters)
    {
        if (!$parameter = $this->findIsFilterParameterById($id)) {
            return;
        }

        if (!$values = $this->findParameterValues($id)) {
            return;
        }

        $result = $this->formatParameter($parameter);

        $selectedValues = $result['active']
            ? explode(',', $this->routeParameters[$result['alias']])
            : array();

        foreach ($values as $value) {
            $result['values'][] = array(
                'active' => in_array($value->getAlias(), $selectedValues),
                'amount' => array_key_exists($value->getId(), $valuesWithAmounts)
                    ? $valuesWithAmounts[$value->getId()]
                    : 0,
                'name'   => (string) $value,
                'alias'  => $value->getAlias(),
                'id'     => $value->getId(),
            );
        }

        $filters[$parameter->getName()] = $result;
    }

    /**
     * Find parameter by id, isFilter and valueType
     *
     * @param string    $id Parameter id
     *
     * @return \Nitra\ProductBundle\Document\Parameter|null Parameter instance or null
     */
    protected function findIsFilterParameterById($id)
    {
        return $this->findParameter('id', $id);
    }

    /**
     * Find parameter by alias, isFilter and valueType
     *
     * @param string    $alias Parameter alias
     *
     * @return \Nitra\ProductBundle\Document\Parameter|null Parameter instance or null
     */
    protected function findIsFilterParameterByAlias($alias)
    {
        return $this->findParameter('aliasEn', $alias);
    }

    /**
     * Find parameter by field - value, isFilter and valueType
     *
     * @param string    $field Field for search
     * @param string    $value Value for search
     *
     * @return \Nitra\ProductBundle\Document\Parameter|null Parameter instance or null
     */
    protected function findParameter($field, $value)
    {
        return $this->dm->createQueryBuilder('NitraProductBundle:Parameter')
            ->field($field)->equals($value)
            ->field('isFilter')->equals(true)
            ->field('valueType')->notEqual('text')
            ->getQuery()->execute()->getSingleResult();
    }

    /**
     * Find parameter values by parameter id and values ids
     *
     * @param string    $id     Parameter id
     * @param array     $ids    Ids of values
     *
     * @return \Nitra\ProductBundle\Document\ParameterValue[] List of parameter values
     */
    protected function findParameterValuesByIds($id, $ids)
    {
        return $this->findParameterValues($id, 'id', $ids);
    }

    /**
     * Find parameter values by parameter id and values ids
     *
     * @param string    $id     Parameter id
     * @param array     $alias  Aliases of values
     *
     * @return \Nitra\ProductBundle\Document\ParameterValue[] List of parameter values
     */
    protected function findParameterValuesByAliases($id, $alias)
    {
        return $this->findParameterValues($id, 'aliasEn', $alias);
    }

    /**
     * Find parameter values by parameter field -value
     *
     * @param string        $id     Parameter id
     * @param string|null   $field  Field for search
     * @param mixed         $value  Value for search
     *
     * @return \Nitra\ProductBundle\Document\ParameterValue[] List of parameter values
     */
    protected function findParameterValues($id, $field = null, $value = null)
    {
        if (!is_array($value)) {
            $value = array($value);
        }

        $qb =  $this->dm->createQueryBuilder('NitraProductBundle:ParameterValue')
            ->field('parameter.id')->equals($id);
        if ($field) {
            $qb->field($field)->in($value);
        }

        return $qb->sort('sortOrder')
            ->getQuery()->execute();
    }

    /**
     * Format parameter to context
     *
     * @param \Nitra\ProductBundle\Document\Parameter $parameter
     *
     * @return array
     */
    protected function formatParameter($parameter)
    {
        return array(
            'active'         => array_key_exists($parameter->getAlias(), $this->routeParameters),
            'alias'          => $parameter->getAlias(),
            'suffix'         => $parameter->getSuffix(),
            'isSiteMultiple' => $parameter->getIsSiteMultiple(),
            'isFilterShow'   => $parameter->getIsFilterShow(),
            'order'          => $parameter->getSortOrderInFilter(),
        ) + $this->defaultFilterOptions;
    }

    /**
     * Sort filters by order
     *
     * @param array $filters
     */
    protected function sortFilters(&$filters)
    {
        $multisort = array();
        foreach ($filters as $key => $filter) {
            $multisort[$key] = $filter['order'];
        }

        array_multisort($filters, SORT_NUMERIC, $multisort);
    }
}