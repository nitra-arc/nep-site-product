<?php

namespace Nitra\ProductBundle\Breadcrumbs;

class BreadcrumbItem
{
    /**
     * @var string Name of item
     */
    protected $name;

    /**
     * @var string Route name
     */
    protected $route;

    /**
     * @var array Route parameters
     */
    protected $routeParameters;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $route
     * @param array  $routeParameters
     */
    public function __construct($name, $route, array $routeParameters = array())
    {
        $this->name            = (string) $name;
        $this->route           = (string) $route;
        $this->routeParameters = $routeParameters;
    }

    /**
     * Get item name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get item route name
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Get item route parameters
     *
     * @return array
     */
    public function getRouteParameters()
    {
        return $this->routeParameters;
    }
}