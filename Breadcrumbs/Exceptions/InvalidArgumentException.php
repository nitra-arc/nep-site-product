<?php

namespace Nitra\ProductBundle\Breadcrumbs\Exceptions;

class InvalidArgumentException extends \Exception
{
    /**
     * Constructor.
     *
     * @param mixed $argument Argument of <b>render</b> function.<br><b>Not object</b>
     */
    public function __construct($argument)
    {
        $message = 'Method render called with "'
            . gettype($argument)
            . '" argument. Must be object';

        parent::__construct($message, 500, null);
    }
}