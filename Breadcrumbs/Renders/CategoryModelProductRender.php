<?php

namespace Nitra\ProductBundle\Breadcrumbs\Renders;

use Nitra\ProductBundle\Breadcrumbs\AbstractRender;
use Nitra\ProductBundle\Breadcrumbs\BreadcrumbItem;

class CategoryModelProductRender extends AbstractRender
{
    /**
     * {@inheritdoc}
     */
    public function getBreadcrumbs($object)
    {
        $append = array();

        if ($object instanceof \Nitra\ProductBundle\Document\Category) {
            $category = $object;
        } elseif ($object instanceof \Nitra\ProductBundle\Document\Model) {
            $category = $object->getCategory();
            $append   = $this->getAppendForModel($object);
        } elseif ($object instanceof \Nitra\ProductBundle\Document\Product) {
            $category = $object->getModel()->getCategory();
            $append   = $this->getAppendForProduct($object);
        }

        $items = array_reverse($this->getCategoriesBreadcrumbs($category));

        return array_merge($items, $append);
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportClass()
    {
        return array(
            '\Nitra\ProductBundle\Document\Category',
            '\Nitra\ProductBundle\Document\Model',
            '\Nitra\ProductBundle\Document\Product',
        );
    }

    /**
     * Recoursive get breadcrumbs for categories
     *
     * @param \Nitra\ProductBundle\Document\Category    $category
     * @param BreadcrumbItem[]                          $result
     */
    protected function getCategoriesBreadcrumbs($category, array $result = array())
    {
        $result[] = new BreadcrumbItem($category, 'category_page', array(
            'slug' => $category->getAlias(),
        ));

        return $category->getParent()
            ? $this->getCategoriesBreadcrumbs($category->getParent(), $result)
            : $result;
    }

    /**
     * Get append items for model
     *
     * @param \Nitra\ProductBundle\Document\Model $model
     *
     * @return array
     */
    protected function getAppendForModel($model)
    {
        return array(
            new BreadcrumbItem($model, 'model_page', array(
                'alias' => $model->getAlias(),
            )),
        );
    }

    /**
     * Get append items for product
     *
     * @param \Nitra\ProductBundle\Document\Product $product
     *
     * @return array
     */
    protected function getAppendForProduct($product)
    {
        return array(
            new BreadcrumbItem($product->getModel(), 'model_page', array(
                'alias' => $product->getModel()->getAlias(),
            )),
            new BreadcrumbItem($product, 'product_page', array(
                'slug' => $product->getAlias(),
            )),
        );
    }
}