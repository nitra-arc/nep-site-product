<?php

namespace Nitra\ProductBundle\Controller\Brand;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Nitra\StoreBundle\Lib\Globals;

class BrandController extends NitraController
{
    /**
     * Карусель брендов магазина
     *
     * @Template("NitraProductBundle:Brand:carouselBrands.html.twig")
     *
     * @param string    $orientation    Carousel orientation
     * @param integer   $visible        Visible brands
     * @param string    $imageSize      Imagine filter
     *
     * @return array Template parameters
     */
    public function brandsAction($orientation = 'horizontal', $visible = 7, $imageSize = 'brand')
    {
        $brands = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Brand')
            ->field('image')->exists(true)
            ->sort('sortOrder', 'asc')
            ->sort('name', 'asc')
            ->getQuery()->execute();

        return array(
            'brands'      => $brands,
            'orientation' => $orientation,
            'visible'     => $visible,
            'imageSize'   => $imageSize,
        );
    }

    /**
     * Товары категории, сгруппированные по бренду
     *
     * @Template("NitraProductBundle:Brand:brandedProducts.html.twig")
     *
     * @param \Nitra\ProductBundle\Document\Product     $product    Product instance
     * @param integer                                   $limit      Limit of products
     *
     * @return array Template parameters
     */
    public function brandedProductsAction($product, $limit = null)
    {
        // generate cache key
        $cacheKey = $this->mapCacheKey('branded_products') . '_' . $product->getModel()->getCategory()->getId();

        // if cache contains data
        if ($this->getCache()->contains($cacheKey)) {
            // get her
            $items = $this->getCache()->fetch($cacheKey);
        } else {
            // get products limit
            $limit = $limit > 0
                ? $limit
                : Globals::getStoreLimit('product_catalog_block', 10);

            // get products by model category
            $products = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')
                ->getProductsByCategoryQb($product->getModel()->getCategory())
                ->getQuery()->execute();

            // format items
            $items = $this->formatBrandedProducts($products, $limit);

            // save to cache
            $this->getCache()->save($cacheKey, $items);
        }

        // return template parameters
        return array(
            'items'   => $items,
            'product' => $product,
        );
    }

    /**
     * Format branded products for tempalte and cache
     *
     * @param \Nitra\ProductBundle\Document\Product[]   $products   Products inastances
     * @param integer                                   $limit      Amount of products of one brand
     *
     * @return array    Formatted for template and cache saving products
     */
    protected function formatBrandedProducts($products, $limit)
    {
        // define items array
        $items = array();
        // iterate over the products
        foreach ($products as $product) {
            // get brand from model
            $brand = $product->getModel()->getBrand();

            // if key brand id non exists in items
            if (!array_key_exists($brand->getId(), $items)) {
                // add new item
                $items[$brand->getId()] = array(
                    'brand'    => array(
                        // get brand name
                        'name'  => (string) $brand,
                        // get brand alias for generate link
                        'alias' => $brand->getAlias(),
                    ),
                    // add empty products array
                    'products' => array(),
                );
            }

            // if products amount less limit
            if (count($items[$brand->getId()]['products']) < $limit) {
                // add new product to result array
                $items[$brand->getId()]['products'][$product->getId()] = array(
                    'alias' => $product->getAlias(),
                    'name'  => $product->getFullName(),
                );
            }
        }

        return $items;
    }
}