<?php

namespace Nitra\ProductBundle\Controller\Parameter;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ParameterController extends NitraController
{
    /**
     * Get parameters in short description
     *
     * @Template("NitraProductBundle:Parameter:characteristics.html.twig")
     *
     * @param \Nitra\ProductBundle\Document\Model|\Nitra\ProductBundle\Document\Product $item   Product or model instance
     * @param boolean                                                                   $short  Get short or full characteristics
     * @param string                                                                    $type   Parameters type. Only <b>product, model</b> and <b>all</b> is allowed
     *
     * @return array Template parameters
     */
    public function characteristicsAction($item, $short = false, $type = null)
    {
        $isModel = $item instanceof \Nitra\ProductBundle\Document\Model;

        if (!$type) {
            $type = $isModel
                ? 'model'
                : 'product';
        }

        $product = $isModel
            ? $item->getProducts()->first()
            : $item;

        $items = $product
            ? $this->formatParametersToTemplate($product->getParameters(), $type, $short)
            : array();

        return array(
            'items'   => $items,
            'type'    => $type,
            'product' => $product,
            'isShort' => $short,
        );
    }

    /**
     * Format product or model parameters to template
     *
     * @param \Nitra\ProductBundle\Document\ProductParameter[]  $parameters List of ProductParameter
     * @param string                                            $type       Parameters type
     * @param boolean                                           $short
     * <b>true</b>  - with inShortDescription - true<br>
     * <b>false</b> - with inDescription - true
     *
     * @return array
     */
    protected function formatParametersToTemplate($parameters, $type, $short)
    {
        // define result array
        $formatted = array();

        // iterate parameters
        foreach ($parameters as $productParameter) {
            // get original parameter from product parameter
            $parameter = $productParameter->getParameter();

            // if parameter is match by conditions
            if (!$this->checkParameterToShow($parameter, $type, $short)) {
                // skip parameter
                continue;
            }

            // define values array
            $values = array();
            // iterate parameter values
            foreach ($productParameter->getValues() as $value) {
                // add value to values array
                $values[] = $value;
            }

            // add item to result array
            $formatted[] = array(
                'parameter' => $parameter,
                'values'    => $values,
            );
        }

        return $formatted;
    }

    /**
     * Check parameter match by conditions
     *
     * @param \Nitra\ProductBundle\Document\Parameter   $parameter
     * @param string                                    $type
     * @param boolean                                   $short
     *
     * @return boolean
     */
    protected function checkParameterToShow($parameter, $type, $short)
    {
        // allow by type
        $byType  = in_array($type, array('product', 'model'))
            ? $parameter->getType() == $type
            : true;
        // allow by short description
        $byShort = $short && $parameter->getIsShortDescription();
        // allow by description
        $byDesc  = !$short && $parameter->getIsDescription();

        // true - if type and one of descriptions
        return $byType && ($byShort || $byDesc);
    }
}