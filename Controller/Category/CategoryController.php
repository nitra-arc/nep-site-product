<?php

namespace Nitra\ProductBundle\Controller\Category;

use Nitra\ProductBundle\Document\Category;
use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Nitra\StoreBundle\Lib\Globals;

class CategoryController extends NitraController
{
    /**
     * @return \Nitra\ProductBundle\Lib\Filter Filter instance
     */
    protected function getFilter()
    {
        return $this->get('nitra.filter');
    }

    /**
     * Получение категорий для топ-меню
     *
     * @Template("NitraProductBundle:Category:categoryMenu.html.twig")
     *
     * @param string|null   $categoryId     Id of category
     *
     * @return array Template parameters
     */
    public function categoryMenuAction($categoryId = null)
    {
        $repository         = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Category');
        $category           = $repository->find($categoryId);
        $childrenCategories = $repository->childrenHierarchyWithSortOrder($category);

        return array(
            'categories' => $childrenCategories,
        );
    }

    /**
     * Получение категорий для меню на главной
     *
     * @Template("NitraProductBundle:Category:categoryMenuHome.html.twig")
     *
     * @return array Template parameters
     */
    public function categoryMenuHomeAction()
    {
        $categories = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Category')
            ->childrenHierarchyWithSortOrder();

        return array(
            'categories' => $categories,
        );
    }

    /**
     * Получение категорий для футера
     *
     * @Template("NitraProductBundle:Category:categoryMenuFooter.html.twig")
     *
     * @return array Template parameters
     */
    public function categoryMenuFooterAction()
    {
        $categories = $this->getDocumentManager()
            ->getRepository('NitraProductBundle:Category')
            ->getCategoriesTree(null);

        return array(
            'categories' => $categories,
        );
    }

    /**
     * @Route("/get-dynamic-link", name="get_dynamic_link")
     */
    public function dynamicRoute(Request $request)
    {
        $routeName       = $request->request->get('name');
        $routeParameters = $request->request->get('parameters');

        return new Response(urldecode($this->generateUrl($routeName, $routeParameters)));
    }

    /**
     * @Route("/category/{slug}/{sort}", name="category_page", defaults={"sort"="name"})
     * @Template("NitraProductBundle:Category:categoryPage.html.twig")
     * @Cache(smaxage="300", public="true")
     */
    public function categoryPageAction(Request $request, $slug, $sort)
    {
        $mode     = $this->container->getParameter('nitra.filter.mode');

        $category = $this->findCategory($slug);

        if (!$category) {
            throw $this->createNotFoundException(sprintf('Category by alias "%s" not found', $slug));
        }

        $filter = $this->getFilter();
        $qb     = $filter->filter($category);
        $qb->sort($this->getFilterSorts($sort));

        $routeParameters = array_merge(
            $request->query->all(),
            $request->attributes->get('_route_params')
        );

        $children = $this->getChildrenCategories($category->getId());

        return array(
            'mode'            => $mode,
            'category'        => $category,
            'children'        => $children,
            'items'           => $this->paginate($qb),
            'routeParameters' => $routeParameters,
        );
    }

    /**
     * @Route("/ajax-filter/{alias}", name="ajax_filter")
     * Method("POST")
     *
     * @param \Symfony\Component\HttpFoundation\Request     $request    Request instance
     * @param string                                        $alias      Alias of category
     *
     * @return array Template parameters
     */
    public function loadAjaxFilter(Request $request, $alias)
    {
        foreach ($request->request->get('parameters', array()) as $parameter => $values) {
            $request->query->set($parameter, implode(',', $values));
        }

        $category = $this->findCategory($alias);
        $filter   = $this->getFilter();

        return new JsonResponse($filter->ajax($category));
    }

    /**
     * Find category by alias
     *
     * @param string $alias
     *
     * @return \Nitra\ProductBundle\Document\Category|null
     */
    protected function findCategory($alias)
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Category')
            ->field('aliasEn')->equals(mb_strtolower($alias, 'UTF-8'))
            ->getQuery()->execute()
            ->getSingleResult();
    }

    /**
     * Get children categories
     *
     * @param string $parentId  Parent category id
     *
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    protected function getChildrenCategories($parentId)
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Category')
            ->field('parent.id')->equals($parentId)
            ->sort('sortOrder')
            ->getQuery()->execute();
    }

    /**
     * Get sort array
     *
     * @param string $sort
     *
     * @return array
     */
    protected function getFilterSorts($sort)
    {
        $sorts = array();
        $store = $this->getStore();
        $mode  = $this->container->getParameter('nitra.filter.mode');

        switch ($mode) {
            case 'product':
                $badgeSorts = $this->getBadgeSorts();

                switch ($sort) {
                    case 'name':
                        $sorts['fullNames.' . $store['id']]  = 1;
                        break;
                    case 'price':
                        $sorts['storePrice.' . $store['id'] . '.price'] = 1;
                        break;
                    default:
                        if (in_array($sort, $badgeSorts)) {
                            $sorts['badgeSorts.' . $sort] = -1;
                            $sorts['storePrice.' . $store['id'] . '.price'] = 1;
                        }
                        break;
                }
                break;
            case 'model':
                switch ($sort) {
                    case 'name':
                        $sorts['fullNames.' . $store['id']]  = 1;
                        break;
                    case 'price':
                        $sorts['prices.' . $store['id'] . '.from'] = 1;
                        $sorts['prices.' . $store['id'] . '.to'] = 1;
                        break;
                }
                break;
        }

        return $sorts;
    }

    /**
     * Get badges for sorting
     *
     * @return array
     */
    protected function getBadgeSorts()
    {
        return $this->getDocumentManager()
            ->createQueryBuilder('NitraProductBundle:Badge')
            ->sort('sortOrder', 1)
            ->distinct('identifier')
            ->getQuery()->execute()->toArray();
    }

    /**
     * Добавление новой страницы категории при скроллинге
     *
     * @Route("/scroller_add_page/{slug}/{sort}", name="scroller_add_page", defaults={"sort"=""})
     * @Template("NitraProductBundle:Aditional:scrollerAddPage.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request     $request    Request instance
     * @param string                                        $slug       Category alias
     * @param string                                        $sort       Sort mode
     *
     * @return array Template parameters
     */
    public function scrollerAddPageAction(Request $request, $slug, $sort)
    {
        $data = $this->getFilteredData($slug, false);

        $query = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product')->setQueryArray($data['query']);
        $query->sort($this->getFilterSorts($sort));

        // paginate products
        $products = $this->paginate($query);

        // set route name for paginator
        $products->setUsedRoute('category_page');

        return array(
            'limit'             => $request->getSession()->get(
                'limit',
                $request->get(
                    'limit',
                    Globals::getStoreLimit('page', 10)
                )
            ),
            'products'          => $products,
            'currentPage'       => $request->query->get('page', 1),
        );
    }

    /**
     * Фильтр по категориям
     *
     * @Template("NitraProductBundle:Category:categoryFilter.html.twig")
     *
     * @param string        $routeName          Route name
     * @param array         $routeParameters    Route parameters
     * @param string        $active             Alias(es) of active category or categories (separator - ",")
     * @param array         $ids                Allowed categories ids
     * @param integer       $minLevel           Minimal level for select categories
     * @param boolean       $deactivate         Allow deselect categories
     * @param boolean       $onlyOne            On select two categories - deactivate first
     *
     * @return array Template parameters
     */
    public function categoryFilterAction($routeName, $routeParameters, $active = null, $ids = array(), $minLevel = 0, $deactivate = true, $onlyOne = false)
    {
        $result = array();
        $categories = $this->getCategoriesForFilter($minLevel, $ids);

        foreach ($categories as $category) {
            list ($isActive, $link) = $this->generateLinkToCategoryFilter($category, $routeName, $routeParameters, $active, $deactivate, $onlyOne);

            $result[$category->getAlias()] = array(
                'name'   => $category->getName(),
                'path'   => $link,
                'active' => $isActive,
            );
        }

        return array(
            'categories' => $result,
        );
    }

    /**
     * Get categories for filter
     *
     * @param integer   $minLevel   Minimal level for select
     * @param array     $ids        Allowed categories ids
     *
     * @return \Nitra\ProductBundle\Document\Category[]
     */
    protected function getCategoriesForFilter($minLevel, $ids)
    {
        $qb = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Category')
            ->field('level')->gte($minLevel);
        if ($ids) {
            $qb->field('_id')->in($ids);
        }

        return $qb
            ->sort('path')
            ->getQuery()->execute();
    }

    /**
     * Generate link to page with filter by category
     *
     * @param string    $routeName              Route name
     * @param array     $parameters             Route parameters
     * @param string    $selectedCategories     Alias(es) of active category or categories (separator - ",")
     * @param boolean   $deactivate             Allow deselect categories
     * @param boolean   $onlyOne                On select two categories - deactivate first
     *
     * @return array Is active flag and generated link to page with category filter
     */
    protected function generateLinkToCategoryFilter($category, $routeName, $parameters, $selectedCategories, $deactivate, $onlyOne)
    {
        $currentActive = false;
        $aliases       = $selectedCategories ? explode(',', $selectedCategories) : array();

        if (in_array($category->getAlias(), $aliases)) {
            $currentActive = true;
            unset($aliases[array_search($category->getAlias(), $aliases)]);
        } elseif ($onlyOne) {
            $aliases = array($category->getAlias());
        } else {
            $aliases[] = $category->getAlias();
        }

        if ($aliases) {
            $parameters['category'] = implode(',', $aliases);
        } elseif ($deactivate) {
            unset ($parameters['category']);
        }

        return array(
            $currentActive,
            $this->generateUrl($routeName, $parameters)
        );
    }
}