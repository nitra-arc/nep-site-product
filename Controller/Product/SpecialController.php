<?php

namespace Nitra\ProductBundle\Controller\Product;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Nitra\StoreBundle\Lib\Globals;

class SpecialController extends NitraController
{
    /**
     * Страница просмотра всех акционных товаров
     *
     * @Route("/special_page/{sort}", name="special_page", defaults={"sort"=""})
     * @Template("NitraProductBundle:Modules:specialPage.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string $sort
     *
     * @return array
     */
    public function indexAction(Request $request, $sort)
    {
        $store = $this->getStore();
        $qb    = $this->getDocumentManager()->createQueryBuilder('NitraProductBundle:Product');
        $expr  = $qb->expr()
            ->field('storePrice.' . $store['id'] . '.discount')->gt(0);

        $qb ->addOr($expr)
            ->sort($this->getSort($sort));

        $this->actionProducts($qb);

        $products = $this->paginate($qb);

        return array(
            'products'  => $products,
        );
    }

    /**
     * Get sort for query
     *
     * @param string $sort
     *
     * @return array
     */
    protected function getSort($sort)
    {
        $store = $this->getStore();
        $sorts = array();

        switch ($sort) {
            case 'name':
                $sorts['fullNames.' . $store['id']] = 1;
                break;
            default:
                $sorts['storePrice.' . $store['id'] . '.price'] = 1;
                break;
        }

        return $sorts;
    }

    /**
     * Добавление к выборке товаров, подпадающих под акции
     *
     * @param \Doctrine\ODM\MongoDB\Query\Builder $qb
     */
    protected function actionProducts($qb)
    {
        $key   = $this->mapCacheKey('nitra_actions_by_product_ids');
        $cache = $this->getCache();
        $ids   = array();
        if ($cache->contains($key)) {
            foreach ($cache->fetch($key) as $id) {
                $ids[] = new \MongoId($id);
            }
        }

        $expr = $qb->expr()->field('id')->in($ids);
        $qb->addOr($expr);
    }
}