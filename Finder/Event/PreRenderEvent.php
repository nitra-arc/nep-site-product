<?php

namespace Nitra\ProductBundle\Finder\Event;

use Symfony\Component\EventDispatcher\Event;

class PreRenderEvent extends Event
{
    /**
     * @var string      called type, one of:
     * - groups.item    - render one item from group page
     * - groups         - render groups page
     * - group          - render result page of one group
     * - no.results     - render not results page
     */
    protected $type;

    /** @var string */
    protected $template;
    /** @var array */
    protected $parameters;
    /** @var string */
    protected $group;
    /** @var array */
    protected $configuration;

    public function __construct($type, &$template, &$parameters, $group = null, $groupConfiguration = null)
    {
        $this->type             = $type;
        $this->template         = &$template;
        $this->parameters       = &$parameters;
        $this->group            = $group;
        $this->configuration    = $groupConfiguration;
    }

    /**
     * get paginated group
     * @return string
     */
    public function getGroupName()
    {
        if (in_array($this->type, array('groups', 'no.results'))) {
            throw new \Exception("Method getGroupName not allowed for \"{$this->type}\"");
        }

        return $this->group;
    }

    /**
     * get configuration of group
     * @return array
     */
    public function getGroupConfiguration()
    {
        if (in_array($this->type, array('groups', 'no.results'))) {
            throw new \Exception("Method getGroupName not allowed for \"{$this->type}\"");
        }

        return $this->configuration;
    }

    /**
     * getting render template
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * getting render parameters
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * setting render parameters
     * @param array $parameters
     * @return self
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * getting event type
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}