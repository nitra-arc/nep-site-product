<?php

namespace Nitra\ProductBundle\Finder;

final class FinderEvents
{
    /**
     * The event is thrown each time an call paginate of group
     * in the system.
     *
     * The event listener receives an
     * Nitra\ProductBundle\Finder\Event\PaginateEvent instance.
     *
     * @var string
     */
    const ON_PAGINATE = 'nitra.finder.on.paginate';

    /**
     * The event is thrown each time an call render template
     * in the system.
     *
     * The event listener receives an
     * Nitra\ProductBundle\Finder\Event\PreRenderEvent instance.
     *
     * @var string
     */
    const PRE_RENDER = 'nitra.finder.pre.render';
}