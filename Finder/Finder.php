<?php

namespace Nitra\ProductBundle\Finder;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;
use Elasticsearch\Client;
use Nitra\ProductBundle\Finder\FinderEvents;
use Nitra\ProductBundle\Finder\Event\PaginateEvent;
use Nitra\ProductBundle\Finder\Event\PreRenderEvent;
use Nitra\StoreBundle\Lib\Globals;

class Finder
{
    /** @var \Symfony\Component\DependencyInjection\Container */
    protected $container;

    /** @var \Elasticsearch\Client */
    protected $client;

    /** @var \Twig_Environment */
    protected $twig;

    /** @var \Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher */
    protected $dispacher;

    // array with configuration
    protected $configuration;
    // query term
    protected $term;
    // selected group
    protected $group;

    // regex to search odm
    protected $regex;

    // amount items by group
    protected $amounts = array();

    /**
     * @param \Symfony\Component\DependencyInjection\Container $container
     * @param array $configuration
     */
    public function __construct(Container $container, array $configuration)
    {
        $this->container        = $container;
        $this->configuration    = $configuration;
        $this->twig             = $container->get('twig');
        $this->dispacher        = $container->get('event_dispatcher');
    }

    /**
     * @param string $term
     * @param bool $suggest
     * @param string $group
     * @param string $sort
     * @return Response
     */
    public function search($term, $suggest = false, $group = null, $sort = "name")
    {
        if (!mb_detect_encoding($term, 'UTF-8', true)) {
            $term = iconv('Windows-1251', 'UTF-8', $term);
        }
        $this->term     = $term;
        $this->group    = $group;
        // explode by space query string
        $keyWords = explode(' ', preg_replace('/[^A-ZА-ЯЁ0-9\s]/iu', ' ', mb_strtolower($term, 'UTF-8')));

        // getting query array from elasticsearch (if enabled) or database
        $queryArrays = $this->useElasticSearch()
            ? $this->searchElastic($keyWords)
            : $this->searchOdm($keyWords);
        $paginatedResults = $this->paginateResults($queryArrays, $sort);

        if ($suggest) {
            return $paginatedResults;
        }

        // return Response object
        return $this->renderResults($paginatedResults);
    }

    /**
     * Public getter for configuration
     * @param string $group
     */
    public function getConfiguration($group = null)
    {
        if ($group) {
            return key_exists($group, $this->configuration['groups'])
                ? $this->configuration['groups'][$group]
                : null;
        }

        return $this->configuration;
    }

    /**
     * Getter for Doctrine MongoDb document manager
     * @return \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected function getDocumentManager()
    {
        return $this->container->get('doctrine_mongodb.odm.document_manager');
    }

    /**
     * Getter for Knp paginator
     * @return \Knp\Component\Pager\Paginator
     */
    protected function getPaginator()
    {
        return $this->container->get('knp_paginator');
    }

    /**
     * @return boolean
     */
    protected function useElasticSearch()
    {
        // if configurated and class exists
        if (key_exists('elasticsearch', $this->configuration) && class_exists('Elasticsearch\Client')) {
            // creating client for host and port from config
            $client = new Client(array(
                'hosts' => array(
                    "{$this->configuration['elasticsearch']['host']}:{$this->configuration['elasticsearch']['port']}",
                ),
            ));

            // if ping is success
            $ping = $client->ping();
            if ($ping) {
                // save client to protected class variable
                $this->client = $client;
            }

            // return ping result
            return $ping;
        }

        return false;
    }

    /**
     * search by elastisearch
     * @param array $keyWords
     * @return array
     */
    protected function searchElastic($keyWords)
    {
        $groups = array();
        foreach ($this->configuration['groups'] as $key => $group) {
            if ($this->configuration['process_all_on_group_select'] || !$this->group || ($this->group == $key)) {
                foreach ($group['odm']['fields'] as $field) {
                    $groups[$key] = $this->getQueryArray($group);
                    $groups[$key]['$or'][] = array(
                        '_id'   => array(
                            '$in'   => $this->getIdsFromElastic($group['es']['type'], $keyWords, $group['es']['fields']),
                        ),
                    );
                }
            }
        }

        return $groups;
    }

    /**
     * get ids from elastic search index
     * @param string $type
     * @param array $keyWords
     * @param array $fields
     * @return \MongoId[]
     */
    protected function getIdsFromElastic($type, $keyWords, $fields)
    {
        $result = $this->getElasticSearchQuery($type, $keyWords, $fields);
        if (!$result['hits']['total']) {
            $result = $this->getElasticSearchQuery($type, $keyWords, $fields, true);
        }

        $ids = array();
        foreach ($result['hits']['hits'] as $hit) {
            $ids[] = new \MongoId($hit['_id']);
        }

        return $ids;
    }

    /**
     * create and send query to ElasticSearch
     * @param string $type
     * @param array $keyWords
     * @param array $fields
     * @param bool $fuzzy
     * @return array
     */
    protected function getElasticSearchQuery($type, $keyWords, $fields, $fuzzy = false)
    {
        $query = array(
            'fields'    => array(),
            'query'     => array(
                'bool'      => array(
                    'must'      => array(),
                ),
            ),
        );
        $must  = $this->generateMatches($keyWords, $fields, $fuzzy);
        $query['query']['bool']['must'] = $must;

        return $this->client->search(array(
            'body'  => $query,
            'index' => $this->configuration['elasticsearch']['index'],
            'type'  => $type,
        ));
    }

    protected function generateMatches($keyWords, $fields, $fuzzy = false)
    {
        $must = array();
        foreach ($keyWords as $word) {
            $queryFields = array();
            foreach ($fields as $field) {
                $queryFields[] = $this->getESField($field, $word, $fuzzy);
            }

            $must[] = array(
                'function_score'    => array(
                    'query'             =>  array(
                        'bool'              => array(
                            'should'            => $queryFields,
                        ),
                    ),
                ),
            );
        }

        return $must;
    }

    protected function getESField($field, $word, $fuzzy)
    {
        if (preg_match('/[0-9]/', $word)) {
            return array('text' => array($field => array(
                'query'     => trim($word),
                'operator'  => 'and',
            ),),);
        } elseif ($fuzzy) {
            return array('fuzzy' => array($field => array(
                'max_expansions'    => 2,
                'value'             => trim($word),
            ),),);
        } else {
            return array('wildcard' => array(
                $field => "*" . trim($word) . "*",
            ),);
        }
    }

    /**
     * search by database (odm)
     * @param array $keyWords
     * @return array
     */
    protected function searchOdm($keyWords)
    {
        $groups = array();
        $this->regex = '/(?=.*' . implode(')(?=.*', $keyWords) . ')/i';
        $regex = new \MongoRegex($this->regex);
        foreach ($this->configuration['groups'] as $key => $group) {
            if ($this->configuration['process_all_on_group_select'] || !$this->group || ($this->group == $key)) {
                $groups[$key] = $this->getQueryArray($group);
                foreach ($group['odm']['fields'] as $field) {
                    $groups[$key]['$or'][] = array(
                        $field  => $regex,
                    );
                }
            }
        }

        return $groups;
    }

    /**
     * Getting query array from repository (if qbGetter is defined in config)
     * @param array $group
     * @return array
     */
    protected function getQueryArray($group)
    {
        $qa = array();
        // if getter for query builder is defined
        if (key_exists('qbGetter', $group) && $group['qbGetter']) {
            // get query array from query builder by configuration
            $qa = $this->getDocumentManager()->getRepository($group['repository'])->{$group['qbGetter']}()->getQueryArray();
            // if key '$or' exists
            if (key_exists('$or', $qa)) {
                // move him to '$and'
                if (!key_exists('$and', $qa)) {
                    $qa['$and'] = array();
                }
                $qa['$and'][] = array('$or' => $qa['$or']);
            }
        } else {
            $qa = array();
        }
        $qa['$or'] = array();

        return $qa;
    }

    /**
     * paginate each group
     * @param array $queryArrays
     * @param string $sort
     * @return array
     */
    protected function paginateResults($queryArrays, $sort)
    {
        // define results array
        $results = array();
        foreach ($queryArrays as $key => $queryArray) {
            // getting group configuration
            $groupConfiguration = $this->configuration['groups'][$key];
            // create Query Builder object with sort
            $qb = $this->getDocumentManager()->createQueryBuilder($groupConfiguration['repository'])
                ->setQueryArray($queryArray)
                ->sort($this->getSort($groupConfiguration, $sort));
            // get limit per page (or group)
            $limit = $this->getLimit($groupConfiguration, !$this->group);
            // get array (if embedded) or query builder to paginate
            $toPaginate = $this->checkEmbeddedAndProcessHim($qb, $groupConfiguration);
            // create on paginate event
            $event = new PaginateEvent($key, $groupConfiguration, $toPaginate, $limit, $queryArray);
            // dispach event
            $this->dispacher->dispatch(FinderEvents::ON_PAGINATE, $event);
            // save paginated data to result array
            $result = $this->paginate($toPaginate, $limit);
            $this->amounts[$key] = $result->getTotalItemCount();
            // save
            $results[$key] = $result;
        }

        return $results;
    }

    protected function checkEmbeddedAndProcessHim($qb, $groupConfiguration)
    {
        // set qb as variable wich was be paginated
        $toPaginate = $qb;
        // if embedded is enabled
        if ($groupConfiguration['embedded']) {
            // define results array
            $embeddedResult = array();
            // getting items
            $items = $qb->getQuery()->execute();
            // loop by items
            foreach ($items as $item) {
                // getting embedded data
                $embeddedData = $item->{'get' . ucfirst($groupConfiguration['embedded'])}();
                foreach ($embeddedData as $embedded) {
                    if ($this->isMatchEmbedded($embedded, $groupConfiguration['embedded'], $groupConfiguration['odm']['fields'])) {
                        $embeddedResult[] = array(
                            'embedded'  => $embedded,
                            'item'      => $item,
                        );
                    }
                }
            }
            // set array with embedded documents to variable wich was be paginated
            $toPaginate = $embeddedResult;
        }

        return $toPaginate;
    }

    protected function isMatchEmbedded($object, $embedded, $fields)
    {
        foreach ($fields as $field) {
            if (preg_match("/{$embedded}/", $field) && preg_match($this->regex . 'u', $object->{'get' . ucfirst(explode('.', $field)[1])}())) {
                return true;
            }
        }
        return false;
    }

    /**
     * pagination
     * @param \Doctrine\DBAL\Query\QueryBuilder $query
     * @param int|null $limit
     * @param int $page
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    protected function paginate($query, $limit, $page = 1)
    {
        $pagination = $this->getPaginator()->paginate(
            $query,
            $this->container->get('request')->query->get('page', $page),        // page number
            $limit,                                                             // limit per page
            array(
                'sortFieldParameterName' => null                                // for reset automatic sorting by paginator
            )
        );

        return $pagination;
    }

    /**
     * getting limit items from configuration
     * @param array $configuration
     * @param bool $group
     * @return int
     */
    protected function getLimit($configuration, $group = true)
    {
        // if limit is defined in query string
        if ($requestLimit = $this->container->get('request')->query->get('limit')) {
            // return him
            return $requestLimit;
        }
        // if group
        $configLimit = $group
            // if is defined for group - get him, else - get 3
            ? ((key_exists('group', $configuration) && key_exists('limit', $configuration['group']) && $configuration['group']['limit']) ? $configuration['group']['limit'] : 3)
            // if is defined - get him, else - get 10
            : ((key_exists('limit', $configuration) && $configuration['limit']) ? $configuration['limit'] : 10);

        // return integer value
        return (int) is_numeric($configLimit)
            // if is numeric - return him
            ? $configLimit
            // else - getting from store settings
            : Globals::getStoreLimit($configLimit, $group ? 3 : 10);
    }

    /**
     * @param array $paginatedResults
     * @return Response
     */
    protected function renderResults($paginatedResults)
    {
        $sentences = explode(" ", $this->term);
        $replace = array();
        foreach ($sentences as $keyword) {
            if ($keyword) {
                $replace["#{$keyword}#ui"] = "<b>{$keyword}</b>";
            }
        }

        // if not group and results
        if (!$this->group && array_sum($this->amounts)) {
            // return Response
            return $this->renderGroupsPage($paginatedResults, $replace);
        // else if group and results
        } elseif ($this->group && $this->amounts[$this->group]) {
            // return Response
            return $this->renderGroupPage($paginatedResults, $replace);
        // if nothing founded
        } else {
            return $this->renderNoResultsPage();
        }
    }

    /**
     * render page with groups
     * @param \Knp\Component\Pager\Pagination\SlidingPagination[] $paginatedResults
     * @param array $replace
     * @return Response
     */
    protected function renderGroupsPage($paginatedResults, $replace)
    {
        $results = array();
        // render each group
        foreach ($paginatedResults as $key => $group) {
            $groupConfiguration = $this->configuration['groups'][$key];
            $template   = $this->getTemplate($groupConfiguration, true);
            $parameters = array(
                'key'       => $key,
                'results'   => $group,
                'search'    => $this->term,
                'config'    => $groupConfiguration,
            );

            // create pre render event
            $event = new PreRenderEvent('groups.item', $template, $parameters, $key, $groupConfiguration);
            // dispach event
            $this->dispacher->dispatch(FinderEvents::PRE_RENDER, $event);

            $results[$key] = $this->twig->render($template, $parameters);
        }

        $template   = "NitraProductBundle:Search:groupsPage.html.twig";
        $parameters = array(
            'results'   => $results,
            'search'    => $this->term,
            'config'    => $this->configuration,
            'replace'   => $replace
        );

        // create pre render event
        $event = new PreRenderEvent('groups', $template, $parameters);
        // dispach event
        $this->dispacher->dispatch(FinderEvents::PRE_RENDER, $event);

        // render groups page (or suggest)
        $content = $this->twig->render($template, $parameters);

        // return Response
        return new Response($content);
    }

    /**
     * render page of one group
     * @param \Knp\Component\Pager\Pagination\SlidingPagination[] $paginatedResults
     * @param array $replace
     * @return Response
     */
    protected function renderGroupPage($paginatedResults, $replace)
    {
        // getting configuration
        $groupConfiguration = $this->configuration['groups'][$this->group];
        $template   = $this->getTemplate($groupConfiguration, false);
        $parameters = array(
            'results'           => $paginatedResults,
            'search'            => $this->term,
            'config'            => $groupConfiguration,
            'fullConfiguration' => $this->configuration,
            'replace'           => $replace,
            'group'             => $this->group,
        );
        // create pre render event
        $event = new PreRenderEvent('group', $template, $parameters, $this->group, $groupConfiguration);
        // dispach event
        $this->dispacher->dispatch(FinderEvents::PRE_RENDER, $event);

        // render page with results
        $content = $this->twig->render($template, $parameters);

        // return Response
        return new Response($content);
    }

    /**
     * rendering page with no result message
     * @return Response
     */
    protected function renderNoResultsPage()
    {
        $template   = "NitraProductBundle:Search:groupsPage.html.twig";
        $parameters = array(
            'search'    => $this->term,
            'group'     => $this->group,
        );
        // create pre render event
        $event = new PreRenderEvent('no.results', $template, $parameters);
        // dispach event
        $this->dispacher->dispatch(FinderEvents::PRE_RENDER, $event);
        // render page with no results
        $content = $this->twig->render($template, $parameters);

        // return Response
        return new Response($content);
    }

    /**
     * getting template from configuration
     * @param array $configuration
     * @param bool $group
     * @param bool $suggest
     * @return int
     */
    protected function getTemplate($configuration, $group = true, $suggest = false)
    {
        // template getter
        $key = "template";
        // default template for group
        $defaultGroupTemplate = "NitraProductBundle:Search:prototypeGroup.html.twig";
        // default template
        $defaultTemplate = "NitraProductBundle:Search:prototypePage.html.twig";

        // if group
        return $group
            // if is defined for group - get him, else - get default group template
            ? ((key_exists('group', $configuration) && key_exists($key, $configuration['group']) && $configuration['group'][$key]) ? $configuration['group'][$key] : $defaultGroupTemplate)
            // if is defined - get him, else - get default page template
            : ((key_exists($key, $configuration) && $configuration[$key]) ? $configuration[$key] : $defaultTemplate);
    }

    /**
     * getting sort from configuration
     * @param array $configuration
     * @param string $sort
     * @param bool $es
     * @return array
     */
    protected function getSort($configuration, $sort)
    {
        $defaultSort = array(
            'name'  => array(
                'name'  => 1,
            ),
        );
        $orders = array();
        // if config for him is not configured and for group configured
        if (!$orders && key_exists('order', $configuration) && $configuration['order']) {
            $orders = $configuration['order'];
        }

        // if configured for selected order
        $order = key_exists($sort, $orders)
            // get him
            ? $orders[$sort]
            // else get default order by name
            : $defaultSort['name'];

        $resultOrder = array();
        $storeId = Globals::getStore()['id'];
        foreach ($order as $field => $sortOrder) {
            $resultOrder[str_replace('{storeId}', $storeId, $field)] = $sortOrder;
        }

        return $resultOrder;
    }
}