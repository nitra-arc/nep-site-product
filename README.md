# ProductBundle

## Описание
Данный банлд предназначен для работы (вывода, обработки) с:

* товарами (Product) - вся информация о товаре(-х)
* категориями (Category) - категория товара (так-же есть возможность вывода меню (дерева) категорий)
* бейджами (Badge) - аналог состояния товара (акции, новинки, ...)
* брендами (Brand) - бренд товара (производитель)
* цветами (Color) - цвет товара
* параметрами (Parameter) - характеристики товаров
* списоком переводов (Translate) - необходим для поиска (транслитерация - асус(ru) в asus (en))

## Подключение
Для подключения данного модуля в проект необходимо:

* composer.json:

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-site-productbundle": "dev-master",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\ProductBundle\NitraProductBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* app/config/routing.yml:

```yaml
#...
nitra_product:
    prefix: /
    resource: "@NitraProductBundle/Controller/"
    type: annotation
#...
```

## Конфигурация по умолчанию:

```yaml
    # app/config/config.yml

    # ...
    nitra_product:
          # конфигурация фильтра
          filter: ...
          # конфигурация поиска, используя ElasticSearch
          elasticsearch: ...
    # ...
```

## Фильтр
### Конфигурация по умолчанию:

```yaml
    # app/config/config.yml

    # ...
    nitra_product:
          filter:
                template: default
                selected_box: false
                mode: product
    # ...
```

### Описание параметров фильтра:
* template - шаблон для вывода, может быть default и ajax - также можно создать свой шаблон, в папке Filters переопределенного бандла
* selected_box - передавать ли в твиг массив с выбранными параметрами
* mode - тип группировки: 'none' - без группировки, 'model' - по моделям и 'model_with_price' - по моделям с ценой

## Поиск
#### Для работы поиска через **ElasticSearch** необходим модуль: "elasticsearch/elasticsearch"
### Конфигурация по умолчанию:

```yaml
# app/config/config.yml

# ...
nitra_product:
    # ...
    search:
        # Конфигурация ElasticSearch
        elasticsearch:
            # Порт
            port:   '9200'
            # Хост
            host:   'localhost'
            # Индекс
            index:  ''
        # Группы (коллекции) в которых искать
        groups:
            # Группа товаров
            products:
                # Название роута для генерации ссылок
                route: 'product_page'
                # Параметры ссылки
                routeParameters:
                    # параметр роута: геттер документа (alias -> getAlias(), name -> getName())
                    slug: 'alias'
                # количество на странице с результатами
                # может быть как строковое (из настроек магазина) так и числовое
                limit: 'page'
                # Шаблон страницы с результатами
                template: "NitraProductBundle:Search/Product:page.html.twig"
                # Репозиторий документа
                repository: 'NitraProductBundle:Product'
                # Геттер из репозитория, для дополнительных фильтров поиска
                qbGetter: 'getDefaultQb'
                # Сортировка
                order:
                    # Ключ
                    name:
                        # Поле: порядок
                        model: 1
                        name: 1
                    price:
                        'storePrice.{storeId}.price': 1
                # Настройки отображения на странице результатов по группам
                group:
                    # Кол-во
                    limit:      3
                    # шаблон
                    template:   ''
                # Настройки ElasticSearch
                es:
                    # Тип (коллекция в ES)
                    type: 'products'
                    # Поля, по которым вести поиск
                    fields:
                        - fullNameForSearch
                # Настройки ODM
                odm:
                    # Поля, по которым вести поиск
                    fields:
                        - fullNameForSearch
# ...
```

## Популярные товары
Популярные товары - карусель с популярными товарами
### Настройка

```yaml
    # app/config/parameters.yml

    popular_products_mode: by_viewed | by_is_popular
```

* popular_products_mode - способ получения популярных товаров
    * by_viewed - по кол-ву просмотров (по умолчанию)
    * by_is_popular - по галочке в админке

## Популярные модели
Аналогично карусели популярных товаров но "тип" популярности берется исходя их поросмотров либо параметров модели
### Настройка

```yaml
    # app/config/parameters.yml

    popular_models_mode: by_viewed | by_is_popular
```

* popular_products_mode - способ получения популярных моделей
    * by_viewed - по кол-ву просмотров (по умолчанию)
    * by_is_popular - по галочке в админке


# Решение проблемы с переводом переопределенных документов (gedmo, stof)

```yml
#app/config/config.yml

stof_doctrine_extensions:
    # ...
    mongodb:
        default:
            # ...
            translatable:       true
            # ...
    class:
        # ...
        translatable:           Nitra\ProductBundle\Listener\TranslatableListener
        # ...
```